package tools

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

type argumentMode int

const (
	modePosition  argumentMode = 0
	modeImmediate argumentMode = 1
	modeRelative  argumentMode = 2
)

type argument struct {
	mode  argumentMode
	index int
	data  int
}

// IntcodeProgram stores the intcode progrm
type IntcodeProgram struct {
	data []int

	i            int
	relativeBase int
	finished     bool

	iqueue []int
	oqueue []int
}

// NewProgram create a new intcode program
func NewProgram(line string) (*IntcodeProgram, error) {
	data, err := CreateIntcodeData(line)
	if err != nil {
		return nil, err
	}
	return NewProgramFromData(data), nil
}

// NewProgramFromData create a new intcode program from []int
func NewProgramFromData(data []int) *IntcodeProgram {
	p := IntcodeProgram{i: 0, relativeBase: 0, finished: true, iqueue: make([]int, 0), oqueue: make([]int, 0)}
	p.data = make([]int, len(data)+10000)
	copy(p.data, data)
	return &p
}

// CreateIntcodeData get an []int for intcode from a string
func CreateIntcodeData(line string) ([]int, error) {
	var err error
	strNums := strings.Split(line, ",")
	nums := make([]int, len(strNums))
	for i, item := range strNums {
		nums[i], err = strconv.Atoi(item)
		if err != nil {
			return nil, err
		}
	}
	return nums, nil
}

// AddInput adds to the input queue
func (p *IntcodeProgram) AddInput(data int) {
	p.iqueue = append(p.iqueue, data)
}

// RemoveOutput removes and returns data from the output queue
func (p *IntcodeProgram) RemoveOutput() (int, error) {
	if len(p.oqueue) == 0 {
		return -1, errors.New("no data on remove output")
	}
	data := p.oqueue[0]
	p.oqueue = p.oqueue[1:]
	return data, nil
}

// SetData Set data at the index
func (p *IntcodeProgram) SetData(index, data int) {
	p.data[index] = data
}

// GetData Get data at the index
func (p *IntcodeProgram) GetData(index int) int {
	return p.data[index]
}

// GetStopped get if the program is really done
func (p *IntcodeProgram) GetStopped() bool {
	return p.finished
}

func (p *IntcodeProgram) removeInput() (int, error) {
	if len(p.iqueue) == 0 {
		return -1, errors.New("no data on remove input")
	}
	data := p.iqueue[0]
	p.iqueue = p.iqueue[1:]
	return data, nil
}

func (p *IntcodeProgram) addOutput(data int) {
	p.oqueue = append(p.oqueue, data)
}

func (p *IntcodeProgram) getOP(index int) int {
	return p.data[index] % 100
}

func (p *IntcodeProgram) getArguments(index int, count int) []argument {
	out := make([]argument, count)
	opData := p.data[index] / 100
	for i := 0; i < count; i++ {
		out[i] = argument{mode: argumentMode(opData % 10), index: -1}
		if out[i].mode == modeImmediate {
			out[i].data = p.data[index+1+i]
		} else if out[i].mode == modePosition {
			out[i].index = p.data[index+1+i]
			out[i].data = p.data[out[i].index]
		} else {
			out[i].index = p.relativeBase + p.data[index+1+i]
			out[i].data = p.data[out[i].index]
		}
		opData /= 10
	}
	return out
}

// Run runs the program until it needs more input or finishes
func (p *IntcodeProgram) Run() error {
	p.finished = false
	for p.i < len(p.data) {
		op := p.getOP(p.i)
		switch op {
		case 1: // Add
			args := p.getArguments(p.i, 3)
			if args[2].index == -1 {
				return errors.New("error with the mode: Addition")
			}
			p.data[args[2].index] = args[0].data + args[1].data
			p.i += 4
		case 2: // Multiply
			args := p.getArguments(p.i, 3)
			if args[2].index == -1 {
				return errors.New("error with the mode: Multiplication")
			}
			p.data[args[2].index] = args[0].data * args[1].data

			p.i += 4
		case 3: // Input
			args := p.getArguments(p.i, 1)
			if args[0].index == -1 {
				return errors.New("error with the mode: Input")
			}
			dat, err := p.removeInput()
			if err != nil {
				return nil
			}
			p.data[args[0].index] = dat

			p.i += 2
		case 4: // Output
			args := p.getArguments(p.i, 1)
			p.addOutput(args[0].data)

			p.i += 2
		case 5: // jump-if-true
			args := p.getArguments(p.i, 2)
			if args[0].data != 0 {
				p.i = args[1].data
			} else {
				p.i += 3
			}
		case 6: // jump-if-false
			args := p.getArguments(p.i, 2)
			if args[0].data == 0 {
				p.i = args[1].data
			} else {
				p.i += 3
			}
		case 7: // less than
			args := p.getArguments(p.i, 3)
			if args[2].index == -1 {
				return errors.New("error with the mode")
			}
			if args[0].data < args[1].data {
				p.data[args[2].index] = 1
			} else {
				p.data[args[2].index] = 0
			}

			p.i += 4
		case 8: // equals
			args := p.getArguments(p.i, 3)
			if args[2].index == -1 {
				return errors.New("error with the mode")
			}
			if args[0].data == args[1].data {
				p.data[args[2].index] = 1
			} else {
				p.data[args[2].index] = 0
			}

			p.i += 4
		case 9: // adjusts relative base
			args := p.getArguments(p.i, 1)
			p.relativeBase += args[0].data
			p.i += 2
		case 99:
			p.finished = true
			return nil
		default:
			return fmt.Errorf("unknown op code: %d", op)
		}
	}

	return errors.New("did not hit end instruction and run out of code")
}
