// Package day02 2019
package day02

import "gitlab.com/parkermc/advent-of-code/2019/tools"

const testAnswerOne = "11590668"
const testAnswerTwo = "2254"

var testFilename = "../test/private/" + tools.GetPackageName()
