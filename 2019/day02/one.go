package day02

import (
	"errors"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	program, err := tools.NewProgram(lines[0])
	if err != nil {
		return "", err
	}

	program.SetData(1, 12)
	program.SetData(2, 2)

	err = program.Run()
	if err != nil {
		return "", err
	}

	if !program.GetStopped() {
		return "", errors.New("program not stopped")
	}

	return strconv.Itoa(program.GetData(0)), nil // Return with the answer
}
