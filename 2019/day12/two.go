package day12

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	moons, err := createMoons(lines)
	if err != nil {
		return "", nil
	}

	xP := 0
	yP := 0
	zP := 0
	for i := 1; xP == 0 || yP == 0 || zP == 0; i++ {
		tick(moons)
		x := true
		y := true
		z := true
		for _, m := range moons {
			if x && m.vel.X != 0 {
				x = false
			}
			if y && m.vel.Y != 0 {
				y = false
			}
			if z && m.vel.Z != 0 {
				z = false
			}
		}
		if x {
			xP = i
		}
		if y {
			yP = i
		}
		if z {
			zP = i
		}
	}

	return strconv.Itoa(tools.Lcm(tools.Lcm(xP, yP), zP)), nil // Return with the answer
}
