package day12

import (
	"regexp"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

type moon struct {
	pos tools.Pos3D
	vel tools.Pos3D
}

func createMoons(lines []string) ([]*moon, error) {
	moons := make([]*moon, 0)
	for _, line := range lines {
		r, err := regexp.Compile(`<x=(.*), y=(.*), z=(.*)>`)
		if err != nil {
			return nil, err
		}

		if line != "" {
			x, err := strconv.Atoi(r.FindStringSubmatch(line)[1])
			if err != nil {
				return nil, err
			}
			y, err := strconv.Atoi(r.FindStringSubmatch(line)[2])
			if err != nil {
				return nil, err
			}
			z, err := strconv.Atoi(r.FindStringSubmatch(line)[3])
			if err != nil {
				return nil, err
			}
			moons = append(moons, &moon{pos: tools.Pos3D{X: x, Y: y, Z: z}, vel: tools.Pos3D{X: 0, Y: 0, Z: 0}})
		}
	}
	return moons, nil
}
func tick(moons []*moon) error {
	for i, m := range moons {
		for j, m2 := range moons {
			if i != j {
				if m.pos.X < m2.pos.X {
					m.vel.X++
				} else if m.pos.X > m2.pos.X {
					m.vel.X--
				}
				if m.pos.Y < m2.pos.Y {
					m.vel.Y++
				} else if m.pos.Y > m2.pos.Y {
					m.vel.Y--
				}
				if m.pos.Z < m2.pos.Z {
					m.vel.Z++
				} else if m.pos.Z > m2.pos.Z {
					m.vel.Z--
				}
			}
		}
	}
	for _, m := range moons {
		m.pos = m.pos.Add(m.vel)
	}
	return nil
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	moons, err := createMoons(lines)
	if err != nil {
		return "", nil
	}

	for i := 0; i < 1000; i++ {
		tick(moons)
	}

	energy := 0
	for _, m := range moons {
		energy += (tools.Abs(m.pos.X) + tools.Abs(m.pos.Y) + tools.Abs(m.pos.Z)) * (tools.Abs(m.vel.X) + tools.Abs(m.vel.Y) + tools.Abs(m.vel.Z))
	}

	return strconv.Itoa(energy), nil // Return with the answer
}
