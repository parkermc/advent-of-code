// Package day12 2019
package day12

import "gitlab.com/parkermc/advent-of-code/2019/tools"

const testAnswerOne = "8538"
const testAnswerTwo = "506359021038056"

var testFilename = "../test/private/" + tools.GetPackageName()
