// Package day11 2019
package day11

import "gitlab.com/parkermc/advent-of-code/2019/tools"

const testAnswerOne = "2268"
const testAnswerTwo = "\n" +
	" ##  #### ###  #  # ####   ##  ##  ### \n" +
	"#  # #    #  # # #     #    # #  # #  #\n" +
	"#    ###  #  # ##     #     # #    #  #\n" +
	"#    #    ###  # #   #      # #    ### \n" +
	"#  # #    #    # #  #    #  # #  # # # \n" +
	" ##  #### #    #  # ####  ##   ##  #  #"

var testFilename = "../test/private/" + tools.GetPackageName()
