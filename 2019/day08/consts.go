// Package day08 2019
package day08

import "gitlab.com/parkermc/advent-of-code/2019/tools"

const testAnswerOne = "1474"
const testAnswerTwo = "\n" +
	"  ##  ##  ###   ##  ###  \n" +
	"   # #  # #  # #  # #  # \n" +
	"   # #    #  # #    ###  \n" +
	"   # #    ###  #    #  # \n" +
	"#  # #  # # #  #  # #  # \n" +
	" ##   ##  #  #  ##  ###  \n"

var testFilename = "../test/private/" + tools.GetPackageName()
