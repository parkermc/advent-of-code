package day06

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

type point struct {
	name    string
	orbit   string
	counted bool
}

func getPoints(lines []string) map[string]*point {
	items := make(map[string]*point)
	items["COM"] = &point{name: "COM", orbit: "", counted: true}

	for _, line := range lines {
		split := strings.Split(line, ")")
		items[split[1]] = &point{name: split[1], orbit: split[0], counted: false}
	}

	return items
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	items := getPoints(lines)

	count := 0
	for _, item := range items {
		cur := items[item.orbit]
		for cur != nil {
			count++
			cur = items[cur.orbit]
		}
	}

	return strconv.Itoa(count), nil // Return with the answer
}
