// Package day09 2019
package day09

import "gitlab.com/parkermc/advent-of-code/2019/tools"

const testAnswerOne = "3340912345"
const testAnswerTwo = "51754"

var testFilename = "../test/private/" + tools.GetPackageName()
