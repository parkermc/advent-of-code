// Package main for 2019
package main

import (
	"log"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2019/day01"
	"gitlab.com/parkermc/advent-of-code/2019/day02"
	"gitlab.com/parkermc/advent-of-code/2019/day03"
	"gitlab.com/parkermc/advent-of-code/2019/day04"
	"gitlab.com/parkermc/advent-of-code/2019/day05"
	"gitlab.com/parkermc/advent-of-code/2019/day06"
	"gitlab.com/parkermc/advent-of-code/2019/day07"
	"gitlab.com/parkermc/advent-of-code/2019/day08"
	"gitlab.com/parkermc/advent-of-code/2019/day09"
	"gitlab.com/parkermc/advent-of-code/2019/day10"
	"gitlab.com/parkermc/advent-of-code/2019/day11"
	"gitlab.com/parkermc/advent-of-code/2019/day12"
	"gitlab.com/parkermc/advent-of-code/2019/day13"
	"gitlab.com/parkermc/advent-of-code/2019/day14"
	"gitlab.com/parkermc/advent-of-code/2019/day15"
	"gitlab.com/parkermc/advent-of-code/2019/day16"
	"gitlab.com/parkermc/advent-of-code/2019/day17"
)

const showAll = false

// Puzzle used for the puzzle array
type Puzzle func(string) (string, error)

var puzzles = [][]Puzzle{
	[]Puzzle{day01.One, day01.Two},
	[]Puzzle{day02.One, day02.Two},
	[]Puzzle{day03.One, day03.Two},
	[]Puzzle{day04.One, day04.Two},
	[]Puzzle{day05.One, day05.Two},
	[]Puzzle{day06.One, day06.Two},
	[]Puzzle{day07.One, day07.Two},
	[]Puzzle{day08.One, day08.Two},
	[]Puzzle{day09.One, day09.Two},
	[]Puzzle{day10.One, day10.Two},
	[]Puzzle{day11.One, day11.Two},
	[]Puzzle{day12.One, day12.Two},
	[]Puzzle{day13.One, day13.Two},
	[]Puzzle{day14.One, day14.Two},
	[]Puzzle{day15.One, day15.Two},
	[]Puzzle{day16.One, day16.Two},
	[]Puzzle{day17.One, day17.Two},
}

func main() {
	if showAll {
		for dayNum, dayPuzzles := range puzzles {
			for puzzleNum := range dayPuzzles {
				runPuzzle(dayNum+1, puzzleNum+1)
			}
		}
	} else {
		dayNum := len(puzzles)
		puzzleNum := len(puzzles[dayNum-1])
		runPuzzle(dayNum, puzzleNum)
	}
}

func runPuzzle(dayNum int, puzzleNum int) {
	inputFilename := "input"
	if dayNum < 10 {
		inputFilename = "day0" + strconv.Itoa(dayNum) + "/" + inputFilename
	} else {
		inputFilename = "day" + strconv.Itoa(dayNum) + "/" + inputFilename
	}
	answer, err := puzzles[dayNum-1][puzzleNum-1](inputFilename)
	if err != nil {
		log.Printf("Error with day %d puzzle %d: %s", dayNum, puzzleNum, err)
	}
	log.Printf("Day %d puzzle %d: %s", dayNum, puzzleNum, answer)
}
