package day03

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

type item struct {
	dir    string
	amount int
}

func prepLines(line string) ([]item, error) {
	strNums := strings.Split(line, ",")
	items := make([]item, len(strNums))
	for i, part := range strNums {
		first, rest := tools.TrimFirstChar(part)
		num, err := strconv.Atoi(rest)
		items[i] = item{dir: first, amount: num}
		if err != nil {
			return nil, err
		}
	}
	return items, nil
}

func placeWire(area map[int]map[int]int, line []item, wire int) ([]tools.Pos2D, error) {
	poses := make([]tools.Pos2D, 0)
	x := 0
	y := 0
	for _, part := range line {
		for i := 1; i <= part.amount; i++ {
			switch part.dir {
			case "U":
				y++
			case "D":
				y--
			case "R":
				x++
			case "L":
				x--
			}
			if area[x] != nil && area[x][y] != 0 && area[x][y] != wire {
				poses = append(poses, tools.Pos2D{X: x, Y: y})
			}
			if val, ok := area[x]; !ok || val == nil {
				area[x] = make(map[int]int, 0)
			}
			area[x][y] = wire
		}
	}
	return poses, nil
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	area := make(map[int]map[int]int, 0)
	line1, err := prepLines(lines[0])
	if err != nil {
		return "", err
	}
	line2, err := prepLines(lines[1])
	if err != nil {
		return "", err
	}
	_, err = placeWire(area, line1, 1)
	if err != nil {
		return "", err
	}
	poses, err := placeWire(area, line2, 2)
	if err != nil {
		return "", err
	}

	closestDist := 10000000000
	for _, pos := range poses {
		if val := pos.MDist(&tools.Pos2D{X: 0, Y: 0}); val < closestDist {
			closestDist = val
		}
	}

	return strconv.Itoa(closestDist), nil // Return with the answer
}
