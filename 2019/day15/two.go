package day15

import (
	"container/list"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

func getO2fill(area map[int]map[int]int, start tools.Pos2D) int {
	counted := make(map[int]map[int]bool)
	counted[0] = make(map[int]bool)
	counted[0][0] = true
	queue := list.New()
	queue.PushBack(distEntry{last: start, z: 1})
	lastZ := 0
	for queue.Front() != nil {
		val, _ := queue.Front().Value.(distEntry)
		last := val.last
		z := val.z
		queue.Remove(queue.Front())
		for _, p := range []tools.Pos2D{tools.Pos2D{X: 1, Y: 0}, tools.Pos2D{X: -1, Y: 0}, tools.Pos2D{X: 0, Y: 1}, tools.Pos2D{X: 0, Y: -1}} {
			if (!counted[last.X+p.X][last.Y+p.Y]) && area[last.X+p.X][last.Y+p.Y] != wall {
				if counted[last.X+p.X] == nil {
					counted[last.X+p.X] = make(map[int]bool)
				}
				counted[last.X+p.X][last.Y+p.Y] = true
				queue.PushBack(distEntry{last: tools.Pos2D{X: last.X + p.X, Y: last.Y + p.Y}, z: z + 1})
				lastZ = z
			}
		}

	}
	return lastZ
}

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	area, o2pos, err := createArea(lines)
	if err != nil {
		return "", err
	}

	return strconv.Itoa(getO2fill(area, o2pos)), nil // Return with the answer
}
