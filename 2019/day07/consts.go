// Package day07 2019
package day07

import "gitlab.com/parkermc/advent-of-code/2019/tools"

const testAnswerOne = "914828"
const testAnswerTwo = "17956613"

var testFilename = "../test/private/" + tools.GetPackageName()
