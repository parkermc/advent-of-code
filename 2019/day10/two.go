package day10

import (
	"errors"
	"sort"
	"strconv"

	"github.com/golang-collections/collections/set"
	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

type asteroidEntry struct {
	*asteroid

	angle     float64
	magnitude float64
}

func getLowestMagnitudeIndex(arr []*asteroidEntry) int {
	lowest := 1000000000000.0
	lowestI := -1
	for i, entry := range arr {
		if entry.magnitude < lowest {
			lowest = entry.magnitude
			lowestI = i

		}
	}
	return lowestI
}

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	asteroids := getAsteroids(lines)
	pos, _ := getStationPos(asteroids)

	anglesSet := set.New()
	asteroidMap := make(map[float64][]*asteroidEntry, 0)
	for _, val := range asteroids {
		if val.pos != pos {
			entry := asteroidEntry{asteroid: val, angle: getAngle(pos, val.pos), magnitude: getMag(pos, val.pos)}
			if asteroidMap[entry.angle] == nil {
				asteroidMap[entry.angle] = make([]*asteroidEntry, 0)
			}
			asteroidMap[entry.angle] = append(asteroidMap[entry.angle], &entry)
			anglesSet.Insert(entry.angle)
		}
	}

	angles := make([]float64, anglesSet.Len())
	i := 0
	anglesSet.Do(func(angle interface{}) {
		angles[i] = angle.(float64)
		i++
	})
	sort.Sort((sort.Float64Slice(angles)))

	for i = 0; i < 200; {
		toRemove := []int{}
		for j, angle := range angles {
			if i == 199 {
				index := getLowestMagnitudeIndex(asteroidMap[angle])
				return strconv.Itoa(asteroidMap[angle][index].pos.X*100 + asteroidMap[angle][index].pos.Y), nil
			}
			remove := getLowestMagnitudeIndex(asteroidMap[angle])
			asteroidMap[angle][remove] = asteroidMap[angle][len(asteroidMap[angle])-1]
			asteroidMap[angle][len(asteroidMap[angle])-1] = nil
			asteroidMap[angle] = asteroidMap[angle][:len(asteroidMap[angle])-1]
			i++
			if len(asteroidMap[angle]) == 0 {
				toRemove = append(toRemove, j)
			}

		}
		for _, val := range toRemove {
			angles = append(angles[:val], angles[val+1:]...)
		}
	}

	return "", errors.New("this shouldn't be reached") // Will never get here
}
