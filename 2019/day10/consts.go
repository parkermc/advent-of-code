// Package day10 2019
package day10

import "gitlab.com/parkermc/advent-of-code/2019/tools"

const testAnswerOne = "340"
const testAnswerTwo = "2628"

var testFilename = "../test/private/" + tools.GetPackageName()
