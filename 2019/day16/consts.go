// Package day16 2019
package day16

import "gitlab.com/parkermc/advent-of-code/2019/tools"

const testAnswerOne = "18933364"
const testAnswerTwo = "28872305"

var testFilename = "../test/private/" + tools.GetPackageName()
