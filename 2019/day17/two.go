package day17

import (
	"errors"
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

const (
	condenseMin = 3

	north = 1
	south = 2
	west  = 3
	east  = 4
)

func getPosDir(dir int) tools.Pos2D {
	switch dir {
	case north:
		return tools.Pos2D{Y: -1}
	case south:
		return tools.Pos2D{Y: 1}
	case west:
		return tools.Pos2D{X: -1}
	case east:
		return tools.Pos2D{X: 1}
	}
	return tools.Pos2D{}
}

func getLeft(dir int) int {
	switch dir {
	case north:
		return west
	case south:
		return east
	case west:
		return south
	case east:
		return north
	}
	return -1
}

func getRight(dir int) int {
	switch dir {
	case north:
		return east
	case south:
		return west
	case west:
		return north
	case east:
		return south
	}
	return -1
}

func toArr(str string) []string {
	splitPath := strings.Split(str, ",")
	pathArr := make([]string, 0)
	for i := 0; i < len(splitPath); i++ {
		if splitPath[i] == "A" || splitPath[i] == "B" || splitPath[i] == "C" {
			pathArr = append(pathArr, splitPath[i])
		} else {
			pathArr = append(pathArr, splitPath[i]+","+splitPath[i+1])
			i++
		}
	}
	return pathArr
}

func toStr(a []string) string {
	str := ""
	first := true
	for _, p := range a {
		if first {
			str += p
			first = false
		} else {
			str += "," + p
		}
	}
	return str
}

func count(a, f []string) int {
	return strings.Count(toStr(a), toStr(f))
}

func replace(a, f []string, r string) []string {
	return toArr(strings.ReplaceAll(toStr(a), toStr(f), r))
}

type robot struct {
	pos tools.Pos2D
	dir int
}

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	image, err := getImage(lines[0])
	if err != nil {
		return "", err
	}

	imageL := strings.Split(image, "\n")
	r := robot{}
	for y, row := range imageL {
		for x, val := range row {
			switch val {
			case '^':
				r.pos = tools.Pos2D{X: x, Y: y}
				r.dir = north
			case 'v':
				r.pos = tools.Pos2D{X: x, Y: y}
				r.dir = south
			case '<':
				r.pos = tools.Pos2D{X: x, Y: y}
				r.dir = west
			case '>':
				r.pos = tools.Pos2D{X: x, Y: y}
				r.dir = east
			}
		}
	}

	path := ""
	for {
		if rPos := r.pos.Add(getPosDir(getRight(r.dir))); rPos.Y > -1 && rPos.X > -1 && rPos.Y < len(imageL) && rPos.X < len(imageL[0]) && imageL[rPos.Y][rPos.X] == '#' {
			r.dir = getRight(r.dir)
			path += "R,"
		} else if lPos := r.pos.Add(getPosDir(getLeft(r.dir))); lPos.Y > -1 && lPos.X > -1 && lPos.Y < len(imageL) && lPos.X < len(imageL[0]) && imageL[lPos.Y][lPos.X] == '#' {
			r.dir = getLeft(r.dir)
			path += "L,"
		} else {
			path = path[:len(path)-1]
			break
		}

		dirPos := getPosDir(r.dir)
		dist := 1
		for ; r.pos.Y+dist*dirPos.Y > -1 && r.pos.X+dist*dirPos.X > -1 && r.pos.Y+dist*dirPos.Y < len(imageL) && r.pos.X+dist*dirPos.X < len(imageL[0]) && imageL[r.pos.Y+dist*dirPos.Y][r.pos.X+dist*dirPos.X] == '#'; dist++ {
			continue
		}
		r.pos = r.pos.Add(tools.Pos2D{X: (dist - 1) * dirPos.X, Y: (dist - 1) * dirPos.Y})
		path += strconv.Itoa(dist-1) + ","
	}

	pathArr := toArr(path)

	funcs := make([]string, 0)
	sectionStart := 0
	for j := 0; j < 3; j++ {
		lastCount := -1
		length := 2
		part := make([]string, condenseMin)
		for i := 0; i < condenseMin; i++ {
			length += len(pathArr[sectionStart+i])
			part[i] = pathArr[sectionStart+i]
		}
		lastCount = count(pathArr, part)
		for {
			add := len(pathArr[sectionStart+len(part)]) + 1
			npart := append(part, pathArr[sectionStart+len(part)])
			if length+add > 20 || len(pathArr[sectionStart+len(part)]) == 1 || count(pathArr, npart) != lastCount {
				break
			}
			length += add
			part = npart
		}
		funcs = append(funcs, "")
		first := true
		for _, c := range part {
			if first {
				funcs[len(funcs)-1] += c
				first = false
			} else {
				funcs[len(funcs)-1] += "," + c
			}
		}
		c := ""
		switch j {
		case 0:
			c = "A"
		case 1:
			c = "B"
		case 2:
			c = "C"
		}
		pathArr = replace(pathArr, part, c)
		for ; j < 2 && len(pathArr[sectionStart]) == 1; sectionStart++ {
			continue
		}
	}
	main := toStr(pathArr)

	p, err := tools.NewProgram(lines[0])
	if err != nil {
		return "", err
	}

	p.SetData(0, 2)
	for _, c := range main {
		p.AddInput(int(c))
	}
	p.AddInput('\n')
	for _, a := range funcs {
		for _, c := range a {
			p.AddInput(int(c))
		}
		p.AddInput('\n')
	}

	p.AddInput('n')
	p.AddInput('\n')

	err = p.Run()
	if err != nil {
		return "", err
	}

	if !p.GetStopped() {
		return "", errors.New("program not stopped")
	}

	out := 0
	dat, err := p.RemoveOutput()
	for err == nil {
		if dat > 127 {
			out = dat
		}
		dat, err = p.RemoveOutput()
	}

	return strconv.Itoa(out), nil // Will never get here
}
