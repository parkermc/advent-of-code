package day04

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

func good2(number []int, min, max int) bool {
	if len(number) == 6 && toNum(number) > min && toNum(number) < max {
		for i := 0; i < 5; i++ {
			if number[i] > number[i+1] {
				return false
			}
		}
		for i := 0; i < 5; i++ {
			if number[i] == number[i+1] && ((i-1 < 0 || number[i-1] != number[i]) && (i+2 > 5 || number[i+2] != number[i])) {
				return true
			}
		}
	}
	return false
}

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	split := strings.Split(lines[0], "-")
	min, err := strconv.Atoi(split[0])
	if err != nil {
		return "", nil
	}
	max, err := strconv.Atoi(split[1])
	if err != nil {
		return "", nil
	}

	start := toArray(min)
	end := toArray(max)

	count := 0

	for n1 := start[0]; n1 <= end[0]; n1++ {
		for n2 := n1; n2 < 10; n2++ {
			for n3 := n2; n3 < 10; n3++ {
				for n4 := n3; n4 < 10; n4++ {
					for n5 := n4; n5 < 10; n5++ {
						for n6 := n5; n6 < 10; n6++ {
							if good2([]int{n1, n2, n3, n4, n5, n6}, min, max) {
								count++
							}
						}
					}
				}
			}
		}
	}

	return strconv.Itoa(count), nil // Return with the answer
}
