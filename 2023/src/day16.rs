use std::{
    collections::{HashSet, VecDeque},
    time::Duration,
};

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    let map = input.iter().map(|l| l.chars().collect_vec()).collect_vec();
    let y_len = input.len();
    let x_len = input[0].len();

    solve(
        &map,
        x_len,
        y_len,
        Entry {
            x: 0,
            y: 0,
            direction: Direction::East,
        },
    )
    .to_string()
}

fn part2(input: &Vec<String>) -> String {
    let map = input.iter().map(|l| l.chars().collect_vec()).collect_vec();
    let y_len = input.len();
    let x_len = input[0].len();

    (0..y_len)
        .flat_map(|y| {
            [
                Entry {
                    x: 0,
                    y,
                    direction: Direction::East,
                },
                Entry {
                    x: x_len - 1,
                    y,
                    direction: Direction::West,
                },
            ]
        })
        .chain((0..x_len).flat_map(|x| {
            [
                Entry {
                    x,
                    y: 0,
                    direction: Direction::South,
                },
                Entry {
                    x,
                    y: y_len - 1,
                    direction: Direction::North,
                },
            ]
        }))
        .map(|e| solve(&map, x_len, y_len, e))
        .max()
        .expect("Expected max")
        .to_string()
}

fn solve(map: &Vec<Vec<char>>, x_len: usize, y_len: usize, start: Entry) -> usize {
    let mut energized = map.iter().map(|l| vec![false; l.len()]).collect_vec();
    let mut queue = VecDeque::from([start]);
    let mut set = HashSet::from([start]);

    while let Some(e) = queue.pop_front() {
        energized[e.y][e.x] = true;
        let c = map[e.y][e.x];

        let direction = match c {
            '|' if e.direction == Direction::East || e.direction == Direction::West => {
                if set.insert(Entry {
                    x: e.x,
                    y: e.y,
                    direction: Direction::North,
                }) {
                    if let Some((nx, ny)) = Direction::South.step(e.x, e.y, x_len, y_len) {
                        let next = Entry {
                            x: nx,
                            y: ny,
                            direction: Direction::South,
                        };
                        set.insert(next);
                        queue.push_back(next);
                    }
                }
                if set.insert(Entry {
                    x: e.x,
                    y: e.y,
                    direction: Direction::South,
                }) {
                    if let Some((nx, ny)) = Direction::North.step(e.x, e.y, x_len, y_len) {
                        let next = Entry {
                            x: nx,
                            y: ny,
                            direction: Direction::North,
                        };
                        set.insert(next);
                        queue.push_back(next);
                    }
                }
                continue;
            }
            '-' if e.direction == Direction::North || e.direction == Direction::South => {
                if set.insert(Entry {
                    x: e.x,
                    y: e.y,
                    direction: Direction::West,
                }) {
                    if let Some((nx, ny)) = Direction::East.step(e.x, e.y, x_len, y_len) {
                        let next = Entry {
                            x: nx,
                            y: ny,
                            direction: Direction::East,
                        };
                        set.insert(next);
                        queue.push_back(next);
                    }
                }
                if set.insert(Entry {
                    x: e.x,
                    y: e.y,
                    direction: Direction::East,
                }) {
                    if let Some((nx, ny)) = Direction::West.step(e.x, e.y, x_len, y_len) {
                        let next = Entry {
                            x: nx,
                            y: ny,
                            direction: Direction::West,
                        };
                        set.insert(next);
                        queue.push_back(next);
                    }
                }
                continue;
            }
            '.' | '|' | '-' => e.direction,
            '/' => match e.direction {
                Direction::North => Direction::East,
                Direction::South => Direction::West,
                Direction::East => Direction::North,
                Direction::West => Direction::South,
            },
            '\\' => match e.direction {
                Direction::North => Direction::West,
                Direction::South => Direction::East,
                Direction::East => Direction::South,
                Direction::West => Direction::North,
            },
            _ => panic!("Not implemented {}", map[e.y][e.x]),
        };
        if set.insert(Entry {
            x: e.x,
            y: e.y,
            direction: direction.opposite(),
        }) {
            if let Some((nx, ny)) = direction.step(e.x, e.y, x_len, y_len) {
                let next = Entry {
                    x: nx,
                    y: ny,
                    direction,
                };
                set.insert(next);
                queue.push_back(next);
            }
        }
    }

    energized
        .into_iter()
        .map(|r| r.into_iter().filter(|v| *v).count())
        .sum::<usize>()
}

#[derive(Hash, PartialEq, Eq, Debug, Clone, Copy)]
struct Entry {
    x: usize,
    y: usize,
    direction: Direction,
}

#[derive(Hash, PartialEq, Eq, Debug, Clone, Copy)]
enum Direction {
    North,
    South,
    East,
    West,
}

impl Direction {
    fn opposite(&self) -> Self {
        match self {
            Self::North => Self::South,
            Self::South => Self::North,
            Self::East => Self::West,
            Self::West => Self::East,
        }
    }

    fn step(&self, x: usize, y: usize, x_len: usize, y_len: usize) -> Option<(usize, usize)> {
        match self {
            Self::North => {
                if y > 0 {
                    Some((x, y - 1))
                } else {
                    None
                }
            }
            Self::South => {
                if y < y_len - 1 {
                    Some((x, y + 1))
                } else {
                    None
                }
            }
            Self::East => {
                if x < x_len - 1 {
                    Some((x + 1, y))
                } else {
                    None
                }
            }
            Self::West => {
                if x > 0 {
                    Some((x - 1, y))
                } else {
                    None
                }
            }
        }
    }
}
