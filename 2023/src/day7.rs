use std::time::Duration;

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    solve(input, false).to_string()
}

fn part2(input: &Vec<String>) -> String {
    solve(input, true).to_string()
}

fn solve(input: &Vec<String>, pt2: bool) -> u64 {
    input
        .iter()
        .map(|l| Hand::from_line(l, pt2))
        .sorted_by_cached_key(|h| {
            h.cards.iter().fold(h.result.get_num() as u64, |v, c| {
                (v << 4) | c.get_num(pt2) as u64
            })
        })
        .enumerate()
        .fold(0, |t, (i, h)| {
            return t + (i as u64 + 1) * h.bid;
        })
}

struct Hand {
    bid: u64,
    cards: Vec<Card>,
    result: Result,
}

impl Hand {
    fn from_line(str: &str, pt2: bool) -> Self {
        let split = str.split(" ").collect_vec();
        let cards = split[0]
            .chars()
            .map(|c| Card::from_char(c).expect("Expected valid card"))
            .collect();
        Hand {
            bid: split[1].parse().expect("Expected bid to be a num"),
            result: Result::calc(&cards, pt2),
            cards,
        }
    }
}

#[derive(PartialEq, Eq, Hash, Clone, Copy)]
enum Card {
    A,
    K,
    Q,
    J,
    T,
    N9,
    N8,
    N7,
    N6,
    N5,
    N4,
    N3,
    N2,
}

impl Card {
    fn get_num(&self, pt2: bool) -> u8 {
        match self {
            Self::A => 14,
            Self::K => 13,
            Self::Q => 12,
            Self::J => {
                if pt2 {
                    1
                } else {
                    11
                }
            }
            Self::T => 10,
            Self::N9 => 9,
            Self::N8 => 8,
            Self::N7 => 7,
            Self::N6 => 6,
            Self::N5 => 5,
            Self::N4 => 4,
            Self::N3 => 3,
            Self::N2 => 2,
        }
    }

    fn from_char(c: char) -> Option<Self> {
        match c {
            'A' => Some(Self::A),
            'K' => Some(Self::K),
            'Q' => Some(Self::Q),
            'J' => Some(Self::J),
            'T' => Some(Self::T),
            '9' => Some(Self::N9),
            '8' => Some(Self::N8),
            '7' => Some(Self::N7),
            '6' => Some(Self::N6),
            '5' => Some(Self::N5),
            '4' => Some(Self::N4),
            '3' => Some(Self::N3),
            '2' => Some(Self::N2),
            _ => None,
        }
    }
}

enum Result {
    K5,
    K4,
    FH,
    K3,
    TP,
    OP,
    HC,
}

impl Result {
    fn get_num(&self) -> u8 {
        match self {
            Self::K5 => 7,
            Self::K4 => 6,
            Self::FH => 5,
            Self::K3 => 4,
            Self::TP => 3,
            Self::OP => 2,
            Self::HC => 1,
        }
    }
    fn calc(cards: &Vec<Card>, pt2: bool) -> Self {
        let card_count = cards.iter().counts();
        let wilds = if pt2 {
            *card_count.get(&Card::J).unwrap_or(&0)
        } else {
            0
        };
        let max = *card_count
            .iter()
            .filter(|(k, _)| !pt2 || ***k != Card::J)
            .map(|(_, v)| v)
            .max()
            .unwrap_or(&0)
            + wilds;
        if max == 5 {
            return Self::K5;
        }
        if max == 4 {
            return Self::K4;
        }
        if max == 3 {
            let fh_pair_need = if wilds == 0 { 1 } else { 2 };
            if card_count.values().filter(|v| **v == 2).count() == fh_pair_need {
                return Self::FH;
            }
            return Self::K3;
        }
        if max == 2 {
            // No need to handle wilds below as this would become 3 of a kind
            if card_count.values().filter(|c| **c == 2).count() == 2 {
                return Self::TP;
            }
            return Self::OP;
        }

        Self::HC
    }
}
