use std::{
    collections::{hash_map::Entry, HashMap},
    time::Duration,
    usize,
};

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

// 105784
fn part1(input: &Vec<String>) -> String {
    let state = rotate(parse_input(input), true, input[0].len(), input.len());

    state
        .iter()
        .flat_map(|(_, q)| q.iter().filter(|e| e.moveable).map(|e| e.layer_pos + 1))
        .sum::<usize>()
        .to_string()
}

fn part2(input: &Vec<String>) -> String {
    let y_len = input.len();
    let x_len = input[0].len();
    let mut cache = HashMap::new();
    let mut state = parse_input(input);

    let mut found_cache = false;
    let mut i = 0;

    while i < 1000000000 {
        if !found_cache {
            match cache.entry(CacheKey::new(&state)) {
                Entry::Occupied(v) => {
                    let diff = i - v.get();
                    let times = (1000000000 - i) / diff;
                    i = i + diff * times;
                    found_cache = true;
                }
                Entry::Vacant(v) => {
                    v.insert(i);
                    ()
                }
            };
        }
        for (pos, col_len, row_len) in [
            (true, x_len, y_len),
            (false, y_len, x_len),
            (false, x_len, y_len),
            (true, y_len, x_len),
        ] {
            state = rotate(state, pos, col_len, row_len);
        }
        i += 1;
    }

    state
        .iter()
        .flat_map(|(_, q)| q.iter().filter(|e| e.moveable).map(|e| e.row + 1))
        .sum::<usize>()
        .to_string()
}

fn parse_input(input: &Vec<String>) -> HashMap<usize, Vec<MapEntry>> {
    let y_len = input.len();

    input
        .iter()
        .enumerate()
        .flat_map(|(y, row)| {
            row.chars()
                .enumerate()
                .filter(|(_, c)| *c != '.')
                .map(move |(x, c)| MapEntry {
                    moveable: c == 'O',
                    layer_pos: x,
                    row: y_len - y - 1,
                })
        })
        .into_group_map_by(|e| e.row)
}

fn rotate(
    input: HashMap<usize, Vec<MapEntry>>,
    pos: bool,
    col_len: usize,
    row_len: usize,
) -> HashMap<usize, Vec<MapEntry>> {
    let mut queues = (0..col_len).map(|_| vec![]).collect_vec(); // Queues to create the rows

    if pos {
        for i in input.keys().sorted().rev() {
            let row = &input[i];
            for e in row {
                if e.moveable {
                    let layer_pos = queues[e.layer_pos]
                        .last()
                        .map(|e: &MapEntry| e.layer_pos - 1)
                        .unwrap_or(row_len - 1);
                    queues[e.layer_pos].push(MapEntry {
                        moveable: true,
                        layer_pos,
                        row: e.layer_pos,
                    })
                } else {
                    queues[e.layer_pos].push(MapEntry {
                        moveable: false,
                        layer_pos: *i,
                        row: e.layer_pos,
                    })
                }
            }
        }
    } else {
        for i in input.keys().sorted() {
            let row = &input[i];
            for e in row {
                if e.moveable {
                    let layer_pos = queues[e.layer_pos]
                        .last()
                        .map(|e: &MapEntry| e.layer_pos + 1)
                        .unwrap_or(0);
                    queues[e.layer_pos].push(MapEntry {
                        moveable: true,
                        layer_pos,
                        row: e.layer_pos,
                    })
                } else {
                    queues[e.layer_pos].push(MapEntry {
                        moveable: false,
                        layer_pos: *i,
                        row: e.layer_pos,
                    })
                }
            }
        }
    }
    queues.into_iter().flatten().into_group_map_by(|e| e.row)
}

struct MapEntry {
    moveable: bool,
    layer_pos: usize,
    row: usize,
}

#[derive(PartialEq, Eq, Hash)]
struct CacheKey {
    points: Vec<usize>,
}

impl CacheKey {
    fn new(input: &HashMap<usize, Vec<MapEntry>>) -> Self {
        CacheKey {
            points: input
                .values()
                .flatten()
                .map(|e| e.layer_pos * 1000 + e.row)
                .sorted()
                .collect_vec(),
        }
    }
}
