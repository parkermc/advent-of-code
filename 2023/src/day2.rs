use std::time::Duration;

use itertools::MultiUnzip;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    find_maxes(input)
        .enumerate()
        .map(|(i, (r, g, b))| {
            if r > 12 || g > 13 || b > 14 {
                return 0;
            }
            i + 1
        })
        .sum::<usize>()
        .to_string()
}

fn part2(input: &Vec<String>) -> String {
    find_maxes(input)
        .map(|(r, g, b)| r as u64 * g as u64 * b as u64)
        .sum::<u64>()
        .to_string()
}

fn find_maxes(input: &Vec<String>) -> impl Iterator<Item = (u8, u8, u8)> + '_ {
    input.iter().map(|l| {
        let (r, g, b): (Vec<u8>, Vec<u8>, Vec<u8>) = l
            .split(": ")
            .last()
            .expect("Expected last")
            .split("; ")
            .flat_map(|g| g.split(", "))
            .map(|c| -> (u8, u8, u8) {
                let split: Vec<_> = c.split(" ").collect();
                let num = split[0].parse::<u8>().expect("Expected number");
                match split[1] {
                    "red" => (num, 0, 0),
                    "green" => (0, num, 0),
                    "blue" => (0, 0, num),
                    _ => panic!("Unknown color {}", split[1]),
                }
            })
            .multiunzip();
        (
            *r.iter().max().expect("Expected R max"),
            *g.iter().max().expect("Expected G max"),
            *b.iter().max().expect("Expected B max"),
        )
    })
}
