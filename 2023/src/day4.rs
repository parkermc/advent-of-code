use std::time::Duration;

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn get_match_counts(input: &Vec<String>) -> impl Iterator<Item = usize> + '_ {
    input.iter().map(|l| {
        let split = l
            .split(": ")
            .last()
            .expect("Failed to split")
            .split(" | ")
            .collect_vec();
        let winning_nums: Vec<u64> = split[0]
            .split(" ")
            .filter(|s| !s.is_empty())
            .map(|n| n.parse().expect("Expected number"))
            .collect_vec();
        let match_count = split[1]
            .split(" ")
            .filter(|s| !s.is_empty())
            .map(|n| n.parse().expect("Expected number"))
            .filter(|n| winning_nums.contains(n))
            .count();
        match_count
    })
}

fn part1(input: &Vec<String>) -> String {
    get_match_counts(input)
        .map(|c| {
            if c == 0 {
                return 0;
            }
            2_u64.pow(c as u32 - 1)
        })
        .sum::<u64>()
        .to_string()
}

fn part2(input: &Vec<String>) -> String {
    let mut card_counts: Vec<u64> = vec![1; input.len()];

    for (i, matches) in get_match_counts(input).enumerate() {
        let count = card_counts[i];
        for card in i + 1..=(i + matches) {
            card_counts[card] += count;
        }
    }
    card_counts.iter().sum::<u64>().to_string()
}
