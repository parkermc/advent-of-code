use std::time::Duration;

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn solve(row: &Vec<i64>, backwards: bool) -> i64 {
    let next_row = (1..row.len()).map(|i| (row[i] - row[i - 1])).collect_vec();

    if Ok(&0) == next_row.iter().all_equal_value() {
        if backwards {
            return row[0];
        }
        return *row.last().expect("Expected last");
    }

    if backwards {
        return row[0] - solve(&next_row, backwards);
    }
    row.last().expect("Expected last") + solve(&next_row, backwards)
}

fn part1(input: &Vec<String>) -> String {
    input
        .iter()
        .map(|l| {
            let row = l
                .split(' ')
                .map(|n| n.parse().expect("Expected number in input"))
                .collect_vec();
            solve(&row, false)
        })
        .sum::<i64>()
        .to_string()
}

fn part2(input: &Vec<String>) -> String {
    input
        .iter()
        .map(|l| {
            let row = l
                .split(' ')
                .map(|n| n.parse().expect("Expected number in input"))
                .collect_vec();
            solve(&row, true)
        })
        .sum::<i64>()
        .to_string()
}
