use std::{collections::VecDeque, time::Duration, u32};

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    let mut x_max = 70;
    let mut y_max = 70;
    let mut skip = 0;
    let mut take = 1024;
    if input[2].starts_with("//") {
        (y_max, x_max) = input[0]
            .split(',')
            .map(|s| s.parse::<usize>().unwrap())
            .collect_tuple()
            .unwrap();
        take = input[1].parse::<usize>().unwrap();
        skip = 3;
    }
    let mut map = (0..=y_max).map(|_| vec![u32::MAX; x_max + 1]).collect_vec();
    input.into_iter().skip(skip).take(take).for_each(|l| {
        let (y, x) = l
            .split(',')
            .map(|s| s.parse::<usize>().unwrap())
            .collect_tuple()
            .unwrap();
        map[y][x] = 0;
    });

    solve(map, y_max, x_max).unwrap()
}

fn part2(input: &Vec<String>) -> String {
    let mut x_max = 70;
    let mut y_max = 70;
    let mut skip = 0;
    if input[2].starts_with("//") {
        (y_max, x_max) = input[0]
            .split(',')
            .map(|s| s.parse::<usize>().unwrap())
            .collect_tuple()
            .unwrap();
        skip = 3;
    }
    let mut map = (0..=y_max).map(|_| vec![u32::MAX; x_max + 1]).collect_vec();
    let mut blocks = input
        .into_iter()
        .skip(skip)
        .map(|l| {
            return l
                .split(',')
                .map(|s| s.parse::<usize>().unwrap())
                .collect_tuple::<(usize, usize)>()
                .unwrap();
        })
        .collect_vec();
    let mut end = blocks.len();

    while end > 2 {
        let mut sub_map = map.clone();
        blocks
            .iter()
            .take(end / 2)
            .for_each(|(y, x)| sub_map[*y][*x] = 0);
        match solve(sub_map, y_max, x_max) {
            Some(_) => {
                blocks
                    .iter()
                    .take(end / 2)
                    .for_each(|(y, x)| map[*y][*x] = 0);
                blocks = blocks.split_off(end / 2);
            }
            None => end /= 2,
        }
    }
    let mut pos = 0;
    let block = blocks[pos];
    map[block.0][block.1] = 0;
    while let Some(_) = solve(map.clone(), y_max, x_max) {
        pos += 1;
        let block = blocks[pos];
        map[block.0][block.1] = 0;
    }
    let block = blocks[pos];
    format!("{},{}", block.0, block.1)
}

fn solve(mut map: Vec<Vec<u32>>, y_max: usize, x_max: usize) -> Option<String> {
    let mut queue = VecDeque::new();
    map[0][0] = 0;
    queue.push_back((0, 0));
    while let Some(pos) = queue.pop_front() {
        if pos.0 == x_max && pos.1 == y_max {
            break;
        }
        let steps = map[pos.0][pos.1];
        if pos.0 > 0 && map[pos.0 - 1][pos.1] > steps + 1 {
            map[pos.0 - 1][pos.1] = steps + 1;
            queue.push_back((pos.0 - 1, pos.1));
        }
        if pos.1 > 0 && map[pos.0][pos.1 - 1] > steps + 1 {
            map[pos.0][pos.1 - 1] = steps + 1;
            queue.push_back((pos.0, pos.1 - 1));
        }

        if pos.0 < y_max && map[pos.0 + 1][pos.1] > steps + 1 {
            map[pos.0 + 1][pos.1] = steps + 1;
            queue.push_back((pos.0 + 1, pos.1));
        }
        if pos.1 < x_max && map[pos.0][pos.1 + 1] > steps + 1 {
            map[pos.0][pos.1 + 1] = steps + 1;
            queue.push_back((pos.0, pos.1 + 1));
        }
    }
    if map[y_max][x_max] == u32::MAX {
        return None;
    }
    Some(map[y_max][x_max].to_string())
}
