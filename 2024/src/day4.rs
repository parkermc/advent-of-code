use std::{time::Duration, usize};

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    let grid = input.iter().map(|l| l.chars().collect_vec()).collect_vec();
    let x_len = i32::try_from(grid[0].len()).unwrap();
    let y_len = i32::try_from(grid.len()).unwrap();
    grid.iter()
        .enumerate()
        .map(|(y, l)| {
            l.iter()
                .enumerate()
                .map(|(x, c)| {
                    if *c != 'X' {
                        return 0;
                    }
                    let ix = i32::try_from(x).unwrap();
                    let iy = i32::try_from(y).unwrap();
                    let mut count = 0;
                    for (dx, dy) in [
                        (1, 0),
                        (-1, 0),
                        (0, 1),
                        (0, -1),
                        (1, 1),
                        (-1, 1),
                        (1, -1),
                        (-1, -1),
                    ] {
                        let ex = ix + 3 * dx;
                        let ey = iy + 3 * dy;
                        if ex < 0 || ey < 0 || ex >= x_len || ey >= y_len {
                            continue;
                        }
                        if grid[usize::try_from(ey).unwrap()][usize::try_from(ex).unwrap()] == 'S'
                            && grid[usize::try_from(ey - dy).unwrap()]
                                [usize::try_from(ex - dx).unwrap()]
                                == 'A'
                            && grid[usize::try_from(iy + dy).unwrap()]
                                [usize::try_from(ix + dx).unwrap()]
                                == 'M'
                        {
                            count += 1
                        }
                    }
                    return count;
                })
                .sum::<i64>()
        })
        .sum::<i64>()
        .to_string()
}

fn part2(input: &Vec<String>) -> String {
    let grid = input.iter().map(|l| l.chars().collect_vec()).collect_vec();
    let x_len = i32::try_from(grid[0].len()).unwrap();
    let y_len = i32::try_from(grid.len()).unwrap();
    (grid
        .iter()
        .enumerate()
        .map(|(y, l)| {
            l.iter()
                .enumerate()
                .map(|(x, c)| {
                    if *c != 'M' {
                        return 0;
                    }
                    let ix = i32::try_from(x).unwrap();
                    let iy = i32::try_from(y).unwrap();
                    let mut count = 0;
                    for (dx, dy) in [(1, 1), (-1, 1), (1, -1), (-1, -1)] {
                        let ex = ix + 2 * dx;
                        let ey = iy + 2 * dy;
                        if ex < 0 || ey < 0 || ex >= x_len || ey >= y_len {
                            continue;
                        }
                        // Check current MAS
                        if grid[usize::try_from(ey).unwrap()][usize::try_from(ex).unwrap()] != 'S'
                            || grid[usize::try_from(ey - dy).unwrap()]
                                [usize::try_from(ex - dx).unwrap()]
                                != 'A'
                        {
                            continue;
                        }
                        let others = (
                            grid[usize::try_from(iy).unwrap()][usize::try_from(ex).unwrap()],
                            grid[usize::try_from(ey).unwrap()][usize::try_from(ix).unwrap()],
                        );
                        // check other M and S
                        if (others.0 == 'M' && others.1 == 'S')
                            || (others.0 == 'S' && others.1 == 'M')
                        {
                            count += 1
                        }
                    }
                    return count;
                })
                .sum::<i64>()
        })
        .sum::<i64>()
        / 2)
    .to_string()
}
