use std::time::{Duration, Instant};

pub fn run_day(
    part: Option<u8>,
    module_path: &str,
    part1: fn(&Vec<String>) -> String,
    part2: fn(&Vec<String>) -> String,
    input: &Vec<String>,
) -> Duration {
    let day = module_path
        .split("day")
        .last()
        .expect("module paths should include day");
    return match part {
        Some(p) => match p {
            1 => run_part(day, "1", part1, input),
            2 => run_part(day, "2", part2, input),
            _ => panic!("Unkown part {}", p),
        },
        None => run_part(day, "1", part1, input) + run_part(day, "2", part2, input),
    };
}

fn run_part(
    day: &str,
    part: &str,
    part_fn: fn(&Vec<String>) -> String,
    input: &Vec<String>,
) -> Duration {
    println!("Running Day {} Part {}...", day, part);
    let start = Instant::now();
    let result = part_fn(input);
    let elapsed = start.elapsed();
    println!("Result: {}", result);
    println!("In: {:.2} ms", elapsed.as_micros() as f64 / 1000.0);
    elapsed
}
