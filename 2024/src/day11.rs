use std::{collections::HashMap, time::Duration};

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    solve(input, 25)
}

fn part2(input: &Vec<String>) -> String {
    solve(input, 75)
}
fn solve(input: &Vec<String>, times: u16) -> String {
    let parsed_input = input
        .iter()
        .flat_map(|l| l.split_whitespace().map(|n| n.parse::<u64>().unwrap()))
        .collect_vec();

    let mut cache: HashMap<(u64, u16), u64> = HashMap::new();
    parsed_input
        .into_iter()
        .map(|start| {
            let mut queue = vec![(start, times)];

            while let Some(tuple) = queue.last() {
                let n = tuple.0;
                let left = tuple.1;
                // TODO check cache
                if n == 0 {
                    if left == 1 {
                        cache.insert((n, left), 1);
                        queue.pop();
                        continue;
                    }
                    if let Some(count) = cache.get(&(1, left - 1)) {
                        cache.insert((n, left), *count);
                        queue.pop();
                        continue;
                    }
                    queue.push((1, left - 1));
                    continue;
                }
                let digits = n.checked_ilog10().unwrap_or(0) + 1;
                if digits % 2 == 0 {
                    if left == 1 {
                        cache.insert((n, left), 2);
                        queue.pop();
                        continue;
                    }
                    let str = n.to_string();
                    let split = str.split_at(usize::try_from(digits).unwrap() / 2); // TODO maybe do better
                    let left_d = split.0.parse::<u64>().unwrap();
                    let right_d = split.1.parse::<u64>().unwrap();
                    let left_op = cache.get(&(left_d, left - 1));
                    let right_op = cache.get(&(right_d, left - 1));
                    if left_op == None {
                        queue.push((left_d, left - 1));
                        if right_op == None {
                            queue.push((right_d, left - 1));
                        }
                        continue;
                    }
                    if right_op == None {
                        queue.push((right_d, left - 1));
                        continue;
                    }
                    cache.insert((n, left), right_op.unwrap() + left_op.unwrap());
                    queue.pop();
                    // Add what is not defined yet
                } else {
                    let new = n * 2024;
                    if left == 1 {
                        cache.insert((n, left), 1);
                        queue.pop();
                        continue;
                    }
                    if let Some(count) = cache.get(&(new, left - 1)) {
                        cache.insert((n, left), *count);
                        queue.pop();
                        continue;
                    }
                    queue.push((new, left - 1));
                    continue;
                }
            }
            *cache.get(&(start, times)).unwrap()
        })
        .sum::<u64>()
        .to_string()
}
