use std::time::Duration;

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    let ve = input
        .iter()
        .map(|l| {
            let (one, two) = l
                .split_whitespace()
                .filter(|s| s.len() > 0)
                .filter_map(|s| s.parse::<i64>().ok())
                .collect_tuple()
                .expect("Expected two ints");

            (one, two)
        })
        .collect_vec();
    let onel = ve.iter().map(|(one, _)| one).sorted().collect_vec();
    let twol = ve.iter().map(|(_, two)| two).sorted().collect_vec();
    onel.into_iter()
        .enumerate()
        .map(|(i, v)| (v - twol[i]).abs())
        .sum::<i64>()
        .to_string()
}

fn part2(input: &Vec<String>) -> String {
    let ve = input
        .iter()
        .map(|l| {
            let (one, two) = l
                .split_whitespace()
                .filter(|s| s.len() > 0)
                .filter_map(|s| s.parse::<usize>().ok())
                .collect_tuple()
                .expect("Expected two ints");

            (one, two)
        })
        .collect_vec();
    let onem = ve.iter().map(|(one, _)| one).counts();
    let twom = ve.iter().map(|(_, two)| two).counts();
    onem.into_iter()
        .map(|(k, v)| {
            if let Some(tv) = twom.get(k) {
                return k * v * tv;
            }
            0
        })
        .sum::<usize>()
        .to_string()
}
