use std::{
    collections::{HashMap, HashSet},
    time::Duration,
};

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    solve(input, false)
}

fn part2(input: &Vec<String>) -> String {
    solve(input, true)
}

fn solve(input: &Vec<String>, pt2: bool) -> String {
    let y_len = i32::try_from(input.len()).unwrap();
    let x_len = i32::try_from(input[0].len()).unwrap();
    let mut antennas = HashMap::new();
    input.iter().enumerate().for_each(|(y, line)| {
        line.chars()
            .enumerate()
            .filter(|(_, c)| *c != '.')
            .for_each(|(x, c)| {
                antennas
                    .entry(c)
                    .or_insert(vec![])
                    .push((i32::try_from(y).unwrap(), i32::try_from(x).unwrap()))
            })
    });

    let range = if pt2 { 0..100 } else { 1..2 };

    let antinodes = antennas.values().flat_map(|al| {
        al.iter().enumerate().flat_map(|(skip, a1)| {
            al.iter().skip(skip + 1).flat_map(|a2| {
                let y_dist = a1.0 - a2.0;
                let x_dist = a1.1 - a2.1;
                range
                    .clone()
                    .map(|i| {
                        [
                            (a1.0 + (i * y_dist), a1.1 + (i * x_dist)),
                            (a2.0 - (i * y_dist), a2.1 - (i * x_dist)),
                        ]
                        .into_iter()
                        .filter(|(y, x)| *y > -1 && *x > -1 && *y < y_len && *x < x_len)
                        .collect_vec()
                    })
                    .take_while(|v| v.len() > 0)
                    .flat_map(|v| v)
                    .collect_vec()
            })
        })
    });
    let set: HashSet<_> = antinodes.collect();
    set.iter().count().to_string()
}
