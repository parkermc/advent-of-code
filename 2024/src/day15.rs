use std::{collections::HashSet, time::Duration};

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    let x_len = input[0].len() - 1;
    let y_len = input.iter().find_position(|l| l.is_empty()).unwrap().0 - 1;
    let mut pos = find_chars(input, '@').into_iter().next().unwrap();
    let mut boxes = find_chars(input, 'O');
    let walls = find_chars(input, '#');
    let steps = load_steps(input);

    steps.into_iter().for_each(|d| {
        pos = d
            .step(
                pos,
                &mut boxes,
                &mut HashSet::new(),
                &walls,
                x_len,
                y_len,
                false,
                false,
            )
            .unwrap_or(pos)
    });

    boxes
        .into_iter()
        .map(|b| 100 * b.0 + b.1)
        .sum::<usize>()
        .to_string()
}

fn part2(input: &Vec<String>) -> String {
    let x_len = (input[0].len() - 1) * 2;
    let y_len = input.iter().find_position(|l| l.is_empty()).unwrap().0 - 1;
    let mut pos = find_chars(input, '@').into_iter().next().unwrap();
    pos.1 *= 2;
    let mut left_boxes: HashSet<_> = find_chars(input, 'O')
        .into_iter()
        .map(|p| (p.0, p.1 * 2))
        .collect();
    let mut boxes: HashSet<_> = left_boxes
        .iter()
        .flat_map(|p| [*p, (p.0, p.1 + 1)])
        .collect();
    let walls: HashSet<_> = find_chars(input, '#')
        .iter()
        .flat_map(|p| [(p.0, p.1 * 2), (p.0, p.1 * 2 + 1)])
        .collect();
    let steps = load_steps(input);

    steps.into_iter().for_each(|d| {
        if d.step(
            pos,
            &mut boxes,
            &mut left_boxes,
            &walls,
            x_len,
            y_len,
            true,
            true,
        )
        .is_none()
        {
            return;
        }
        pos = d
            .step(
                pos,
                &mut boxes,
                &mut left_boxes,
                &walls,
                x_len,
                y_len,
                true,
                false,
            )
            .unwrap_or(pos);
    });

    left_boxes
        .into_iter()
        .map(|b| 100 * b.0 + b.1)
        .sum::<usize>()
        .to_string()
}

fn load_steps(input: &Vec<String>) -> Vec<Dir> {
    input
        .iter()
        .skip_while(|l| !l.is_empty())
        .flat_map(|l| {
            l.chars().map(|c| match c {
                '^' => Dir::N,
                'v' => Dir::S,
                '>' => Dir::E,
                '<' => Dir::W,
                _ => panic!("Unknow char {}", c),
            })
        })
        .collect_vec()
}

fn find_chars(input: &Vec<String>, ch: char) -> HashSet<(usize, usize)> {
    skip_last(input.iter().take_while(|l| !l.is_empty()))
        .enumerate()
        .skip(1)
        .flat_map(|(y, l)| {
            skip_last(l.chars())
                .enumerate()
                .skip(1)
                .filter_map(|(x, c)| if c == ch { Some((y, x)) } else { None })
                .collect_vec()
        })
        .collect()
}
fn skip_last<T>(mut iter: impl Iterator<Item = T>) -> impl Iterator<Item = T> {
    let last = iter.next();
    iter.scan(last, |state, item| std::mem::replace(state, Some(item)))
}

#[derive(PartialEq, Eq)]
enum Dir {
    N,
    S,
    E,
    W,
}

impl Dir {
    fn step(
        &self,
        pos: (usize, usize),
        boxes: &mut HashSet<(usize, usize)>,
        left_boxes: &mut HashSet<(usize, usize)>,
        walls: &HashSet<(usize, usize)>,
        x_len: usize,
        y_len: usize,
        pt2: bool,
        dry_run: bool,
    ) -> Option<(usize, usize)> {
        let new_pos = match self {
            Dir::N => {
                if pos.0 == 1 {
                    return None;
                }
                (pos.0 - 1, pos.1)
            }
            Dir::S => {
                if pos.0 + 1 >= y_len {
                    return None;
                }
                (pos.0 + 1, pos.1)
            }
            Dir::E => {
                if pos.1 + 1 >= x_len {
                    return None;
                }
                (pos.0, pos.1 + 1)
            }
            Dir::W => {
                if pos.1 <= if pt2 { 2 } else { 1 } {
                    return None;
                }
                (pos.0, pos.1 - 1)
            }
        };
        if walls.contains(&new_pos) {
            return None;
        }
        if boxes.contains(&new_pos) {
            let new_box_pos = self.step(
                new_pos, boxes, left_boxes, walls, x_len, y_len, pt2, dry_run,
            );
            if new_box_pos.is_none() {
                return None;
            }
            let new_bot_pos = new_box_pos.unwrap();
            if pt2 && (*self == Dir::N || *self == Dir::S) {
                let second_box_pos = if left_boxes.contains(&new_pos) {
                    (new_pos.0, new_pos.1 + 1)
                } else {
                    (new_pos.0, new_pos.1 - 1)
                };
                let new_second_box_pos = self.step(
                    second_box_pos,
                    boxes,
                    left_boxes,
                    walls,
                    x_len,
                    y_len,
                    pt2,
                    dry_run,
                );
                if new_second_box_pos.is_none() {
                    return None;
                }
                let new_second_box_pos = new_second_box_pos.unwrap();
                if !dry_run {
                    if left_boxes.remove(&second_box_pos) {
                        // TODO could improve and not use remove
                        left_boxes.insert(new_second_box_pos);
                    }
                    boxes.remove(&second_box_pos);
                    boxes.insert(new_second_box_pos);
                }
            }

            if !dry_run {
                if pt2 && left_boxes.remove(&new_pos) {
                    left_boxes.insert(new_bot_pos);
                }
                boxes.remove(&new_pos);
                boxes.insert(new_bot_pos);
            }
        }
        Some(new_pos)
    }
}

// fn print_debug(
//     pos: (usize, usize),
//     boxes: &HashSet<(usize, usize)>,
//     left_boxes: &HashSet<(usize, usize)>,
//     walls: &HashSet<(usize, usize)>,
//     x_len: usize,
//     y_len: usize,
//     pt2: bool,
// ) {
//     let x_add = if pt2 { 2 } else { 1 };
//     let mut map = (0..=y_len).map(|_| vec!['.'; x_len + x_add]).collect_vec();
//     map[pos.0][pos.1] = '@';
//     map[0] = vec!['#'; x_len + x_add];
//     map[y_len] = vec!['#'; x_len + x_add];
//     (1..y_len).for_each(|y| {
//         map[y][0] = '#';
//         map[y][x_len] = '#';
//         if pt2 {
//             map[y][1] = '#';
//             map[y][x_len + 1] = '#';
//         }
//     });
//     boxes
//         .iter()
//         .for_each(|(y, x)| map[*y][*x] = if pt2 { ']' } else { 'O' });
//     left_boxes.iter().for_each(|(y, x)| map[*y][*x] = '[');
//     walls.iter().for_each(|(y, x)| map[*y][*x] = '#');
//     println!(
//         "{}",
//         map.into_iter().map(|r| r.into_iter().join("")).join("\n")
//     );
// }
