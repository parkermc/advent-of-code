use std::{
    collections::{BinaryHeap, HashSet},
    time::Duration,
};

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    let (mut grid, mut guard_pos, _, _) = load(input);
    let glen = (grid.len(), grid[0].len());

    let mut dir = Dir::N;
    let mut done = false;
    while !done {
        grid[guard_pos.0][guard_pos.1] = GridCell::Empty(true);
        (guard_pos, dir, done) = dir.move_step(guard_pos, glen, &grid);
    }
    grid.into_iter()
        .map(|r| {
            r.into_iter()
                .filter(|c| {
                    if let GridCell::Empty(seen) = c {
                        return *seen;
                    }
                    false
                })
                .count()
        })
        .sum::<usize>()
        .to_string()
}

fn part2(input: &Vec<String>) -> String {
    let (mut grid, mut guard_pos, mut y_skip_map, mut x_skip_map) = load(input);
    let glen = (grid.len(), grid[0].len());

    let mut loop_count = 0;
    let mut dir = Dir::N;
    let mut seen_no_dir = HashSet::new();
    let mut seen = HashSet::new();
    loop {
        let (next_guard_pos, next_dir, done) = dir.move_step(guard_pos, glen, &grid);
        if done {
            break;
        }
        seen.insert(get_set_key(guard_pos, dir));
        if seen_no_dir.insert(get_set_key(next_guard_pos, Dir::N)) {
            grid[next_guard_pos.0][next_guard_pos.1] = GridCell::Blocked;
            x_skip_map[next_guard_pos.1].push(next_guard_pos.0);
            y_skip_map[next_guard_pos.0].push(next_guard_pos.1);
            let mut loop_seen = seen.clone();
            let mut done = false;
            while !done {
                (guard_pos, dir, done) =
                    dir.move_step_skip(guard_pos, glen, &grid, &y_skip_map, &x_skip_map);
                if !loop_seen.insert(get_set_key(guard_pos, dir)) {
                    loop_count += 1;
                    break;
                }
            }
            grid[next_guard_pos.0][next_guard_pos.1] = GridCell::Empty(true);
            x_skip_map[next_guard_pos.1].retain(|e| *e != next_guard_pos.0);
            y_skip_map[next_guard_pos.0].retain(|e| *e != next_guard_pos.1);
        }
        guard_pos = next_guard_pos;
        dir = next_dir;
    }

    loop_count.to_string()
}

fn load(
    input: &Vec<String>,
) -> (
    Vec<Vec<GridCell>>,
    (usize, usize),
    Vec<BinaryHeap<usize>>,
    Vec<BinaryHeap<usize>>,
) {
    let mut guard_pos: (usize, usize) = (0, 0);
    let mut x_skip_map = vec![];
    for _ in 0..input[0].len() {
        x_skip_map.push(BinaryHeap::new());
    }
    let mut y_skip_map = vec![];
    for _ in 0..input.len() {
        y_skip_map.push(BinaryHeap::new());
    }
    let grid = input
        .iter()
        .enumerate()
        .map(|(y, l)| {
            l.chars()
                .enumerate()
                .map(|(x, c)| match c {
                    '#' => {
                        x_skip_map[x].push(y);
                        y_skip_map[y].push(x);
                        GridCell::Blocked
                    }
                    '.' => GridCell::Empty(false),
                    '^' => {
                        guard_pos = (y, x);
                        GridCell::Empty(true)
                    }
                    _ => panic!("Unknown char {}", c),
                })
                .collect_vec()
        })
        .collect_vec();

    return (grid, guard_pos, y_skip_map, x_skip_map);
}

#[derive(Clone, Copy, PartialEq, Eq)]
enum GridCell {
    Empty(bool),
    Blocked,
}

#[derive(Clone, Copy, PartialEq, Eq)]
enum Dir {
    N,
    S,
    E,
    W,
}

impl Dir {
    fn move_step(
        &self,
        pos: (usize, usize),
        glen: (usize, usize),
        grid: &Vec<Vec<GridCell>>,
    ) -> ((usize, usize), Dir, bool) {
        let mut dir = *self;
        loop {
            let (next_pos, next_done) = dir.get_next(pos, glen);
            if next_done {
                return (next_pos, dir, next_done);
            }

            if let GridCell::Empty(_) = grid[next_pos.0][next_pos.1] {
                return (next_pos, dir, next_done);
            }

            dir = dir.rotate();
        }
    }

    fn move_step_skip(
        &self,
        pos: (usize, usize),
        glen: (usize, usize),
        grid: &Vec<Vec<GridCell>>,
        y_skip_map: &Vec<BinaryHeap<usize>>,
        x_skip_map: &Vec<BinaryHeap<usize>>,
    ) -> ((usize, usize), Dir, bool) {
        let (step_pos, dir, done) = self.move_step(pos, glen, grid);
        if dir != *self {
            return (step_pos, dir, done);
        }

        match self {
            Self::N => {
                if let Some(y) = x_skip_map[pos.1]
                    .clone()
                    .into_sorted_vec()
                    .iter()
                    .rev()
                    .find(|y| **y < pos.0)
                {
                    return ((*y + 1, pos.1), self.rotate(), false);
                }
                ((0, 0), *self, true)
            }
            Self::E => {
                if let Some(x) = y_skip_map[pos.0]
                    .clone()
                    .into_sorted_vec()
                    .iter()
                    .find(|x| **x > pos.1)
                {
                    return ((pos.0, *x - 1), self.rotate(), false);
                }
                ((0, 0), *self, true)
            }
            Self::S => {
                if let Some(y) = x_skip_map[pos.1]
                    .clone()
                    .into_sorted_vec()
                    .iter()
                    .find(|y| **y > pos.0)
                {
                    return ((*y - 1, pos.1), self.rotate(), false);
                }
                ((0, 0), *self, true)
            }
            Self::W => {
                if let Some(x) = y_skip_map[pos.0]
                    .clone()
                    .into_sorted_vec()
                    .iter()
                    .rev()
                    .find(|x| **x < pos.1)
                {
                    return ((pos.0, *x + 1), self.rotate(), false);
                }
                ((0, 0), *self, true)
            }
        }
    }

    fn get_next(&self, pos: (usize, usize), glen: (usize, usize)) -> ((usize, usize), bool) {
        match self {
            Self::N => {
                if pos.0 == 0 {
                    return ((0, 0), true);
                }
                return ((pos.0 - 1, pos.1), false);
            }
            Self::S => {
                if pos.0 + 1 == glen.0 {
                    return ((0, 0), true);
                }
                return ((pos.0 + 1, pos.1), false);
            }
            Self::E => {
                if pos.1 + 1 == glen.1 {
                    return ((0, 0), true);
                }
                return ((pos.0, pos.1 + 1), false);
            }
            Self::W => {
                if pos.1 == 0 {
                    return ((0, 0), true);
                }
                return ((pos.0, pos.1 - 1), false);
            }
        }
    }

    fn rotate(&self) -> Self {
        match self {
            Self::N => Self::E,
            Self::E => Self::S,
            Self::S => Self::W,
            Self::W => Self::N,
        }
    }
}

fn get_set_key(pos: (usize, usize), dir: Dir) -> u64 {
    (((u64::try_from(pos.0).unwrap() << 31) | u64::try_from(pos.1).unwrap()) << 2)
        | match dir {
            Dir::N => 0,
            Dir::S => 1,
            Dir::E => 2,
            Dir::W => 3,
        }
}
