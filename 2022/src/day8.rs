use std::collections::HashSet;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) {
    utils::day::run_day(part, module_path!(), part1, part2, input);
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    let int_arr: Vec<Vec<u8>> = input
        .iter()
        .map(|s| {
            s.chars()
                .map(|c| c.to_string().parse().expect("Expected parseable number"))
                .collect()
        })
        .collect();
    let mut set = HashSet::new();
    let len_y = int_arr.len();
    let len_x = int_arr[0].len();

    for y in 0..len_y {
        let mut highest = 0;
        for x in 0..len_x {
            let num = int_arr[y][x];
            if num > highest || x == 0 {
                set.insert(get_key(x, y));
                highest = num;
            }
        }
    }

    for y in 0..len_y {
        let mut highest = 0;
        for x in (0..len_x).rev() {
            let num = int_arr[y][x];
            if num > highest || x == (len_x - 1) {
                set.insert(get_key(x, y));
                highest = num;
            }
        }
    }

    for x in 0..len_x {
        let mut highest = 0;
        for y in 0..len_y {
            let num = int_arr[y][x];
            if num > highest || y == 0 {
                set.insert(get_key(x, y));
                highest = num;
            }
        }
    }

    for x in 0..len_x {
        let mut highest = 0;
        for y in (0..len_y).rev() {
            let num = int_arr[y][x];
            if num > highest || y == (len_y - 1) {
                set.insert(get_key(x, y));
                highest = num;
            }
        }
    }

    set.len().to_string()
}

fn part2(input: &Vec<String>) -> String {
    let int_arr: Vec<Vec<u8>> = input
        .iter()
        .map(|s| {
            s.chars()
                .map(|c| c.to_string().parse().expect("Expected parseable number"))
                .collect()
        })
        .collect();

    let y_max = int_arr.len();
    let x_max = int_arr[0].len();

    int_arr
        .iter()
        .enumerate()
        .skip(1)
        .map(|(y, line)| {
            if y == y_max - 1 {
                return 0;
            }
            line.iter()
                .enumerate()
                .skip(1)
                .map(|(x, num)| {
                    if x == x_max - 1 {
                        return 0;
                    }
                    let (mut n, mut s, mut e, mut w) = (y, y, x, x);

                    while n > 1 && int_arr[n - 1][x] < *num {
                        n -= 1;
                    }

                    while s < y_max - 2 && int_arr[s + 1][x] < *num {
                        s += 1;
                    }

                    while w > 1 && line[w - 1] < *num {
                        w -= 1;
                    }

                    while e < x_max - 2 && line[e + 1] < *num {
                        e += 1;
                    }

                    (y - n + 1) * (s - y + 1) * (e - x + 1) * (x - w + 1)
                })
                .max()
                .unwrap_or(0)
        })
        .max()
        .expect("Expected max")
        .to_string()
}

fn get_key(x: usize, y: usize) -> usize {
    (x << 32) | y
}
