use std::collections::{hash_map::Entry, HashMap};

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) {
    utils::day::run_day(part, module_path!(), part1, part2, input);
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn load_dirs(input: &Vec<String>) -> Vec<Dir> {
    let mut dirs = vec![Dir {
        parent: 0,
        children: HashMap::new(),
        size: 0,
    }];

    let mut cwd = 0;
    for line in input {
        if let Some(command) = line.strip_prefix("$") {
            let split: Vec<&str> = command.trim().split(" ").collect();
            if split[0] == "cd" {
                match split[1] {
                    ".." => cwd = dirs[cwd].parent,

                    "/" => (),
                    p => cwd = dirs[cwd].children[p],
                }
            }
            continue;
        }

        let split: Vec<&str> = line.trim().split(" ").collect();
        if split[0] == "dir" {
            let i = dirs.len();
            if let Entry::Vacant(v) = dirs[cwd].children.entry(split[1].to_owned()) {
                v.insert(i);
                dirs.push(Dir {
                    parent: cwd,
                    children: HashMap::new(),
                    size: 0,
                });
            }
            continue;
        }
        dirs[cwd].size += split[0].parse::<u64>().expect("Expected number");
    }
    for i in (0..dirs.len()).rev() {
        dirs[i].size += dirs[i]
            .children
            .values()
            .map(|j| dirs[*j].size)
            .sum::<u64>();
    }
    dirs
}
fn part1(input: &Vec<String>) -> String {
    let dirs = load_dirs(input);
    let mut total = 0;
    for dir in dirs {
        if dir.size <= 100000 {
            total += dir.size;
        }
    }
    total.to_string()
}

fn part2(input: &Vec<String>) -> String {
    let dirs = load_dirs(input);
    let to_free = 30000000 - (70000000 - dirs[0].size);

    let mut smallest = 70000000;
    for dir in dirs {
        if dir.size > to_free && dir.size < smallest {
            smallest = dir.size;
        }
    }
    smallest.to_string()
}

struct Dir {
    parent: usize,
    children: HashMap<String, usize>,
    size: u64,
}
