use std::usize;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) {
    utils::day::run_day(part, module_path!(), part1, part2, input);
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn solve(input: &Vec<String>, count: usize) -> usize {
    let chars: Vec<char> = input[0].chars().collect();
    let len = chars.len();
    let mut index = 0;
    while index < len - count {
        let end = index + count - 1;
        index = match (index..end).find(|i| chars[i + 1..=end].contains(&chars[*i])) {
            Some(x) => x + 1,
            None => return index + count,
        };
    }
    panic!("Failed to slove")
}

fn part1(input: &Vec<String>) -> String {
    solve(input, 4).to_string()
}

fn part2(input: &Vec<String>) -> String {
    solve(input, 14).to_string()
}
