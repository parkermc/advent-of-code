package day14

import (
	"math"
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

type response map[rune]int

func (r response) Add(r2 response) response {
	res := make(response)
	for k, v := range r {
		res[k] = v
	}
	for k, v := range r2 {
		res[k] += v
	}
	return res
}

func toMap(data string) response {
	res := make(response)
	for _, r := range data {
		res[r]++
	}
	return res
}

var cache = make(map[string]map[int]response, 0)

func runStepsCached(data string, mappings map[string]string, steps int) response {
	if steps == 0 || len(data) < 2 {
		return toMap(data)
	}

	if steps > 1 {
		if val, ok := cache[data[0:2]]; ok {
			if val2, ok := val[steps]; ok {
				return val2.Add(runStepsCached(data[2:], mappings, steps))
			}
		}
	}

	mid := ""
	if val, ok := mappings[data[:2]]; ok {
		mid = val
	}
	part1 := runStepsCached(data[0:1]+mid, mappings, steps-1) //  run one step down
	part2 := runStepsCached(mid+data[1:2], mappings, steps-1) //  run one step down
	if steps > 1 {
		if _, ok := cache[data[0:1]+mid]; !ok {
			cache[data[0:1]+mid] = make(map[int]response)
		}
		cache[data[0:1]+mid][steps-1] = part1

		if _, ok := cache[mid+data[1:2]]; !ok {
			cache[mid+data[1:2]] = make(map[int]response)
		}
		cache[mid+data[1:2]][steps-1] = part2
	}
	part3 := runStepsCached(data[1:], mappings, steps) // Run at same step

	out := part1.Add(part2).Add(part3)
	out[rune(mid[0])]-- // remove first and last of part 2
	out[rune(data[1])]--
	return out
}

func runPuzzle(lines []string, steps int) int {
	polymer := lines[0]

	mappings := make(map[string]string, 0)
	for _, line := range lines[2:] {
		split := strings.Split(line, " -> ")
		mappings[split[0]] = split[1]
	}

	counts := runStepsCached(polymer, mappings, steps)

	minV := math.MaxInt
	maxV := math.MinInt
	for _, val := range counts {
		if val < minV {
			minV = val
		}
		if val > maxV {
			maxV = val
		}
	}

	return maxV - minV
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	return strconv.Itoa(runPuzzle(lines, 10)), nil // Return with the answer
}
