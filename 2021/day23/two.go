package day23

import (
	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	lines = append(lines, lines[3])
	lines[3] = "  #D#C#B#A#  "
	lines[4] = "  #D#B#A#C#  "
	amphs := make([]*amph, 0)
	for y := 2; y < 6; y++ {
		for _, x := range []int{3, 5, 7, 9} {
			amphs = append(amphs, &amph{
				Type: amphTypes[lines[y][x]],
				Pos:  tools.Pos2D{X: x - 1, Y: y - 1},
			})
		}
	}
	return solve(amphs, 4)
}
