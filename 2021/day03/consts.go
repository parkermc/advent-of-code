// Package day03 2018
package day03

import "gitlab.com/parkermc/advent-of-code/2021/tools"

const testAnswerOne = "1071734"
const testAnswerTwo = "6124992"

var testFilename = "../test/private/" + tools.GetPackageName()
