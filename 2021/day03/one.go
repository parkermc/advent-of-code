package day03

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	counter := make([][]int, len(lines[0]))
	for i := range counter {
		counter[i] = make([]int, 2)
	}

	for _, line := range lines {
		for i, bit := range line {
			counter[i][bit-48]++
		}
	}

	gamma := 0
	epsilon := 0
	for _, val := range counter {
		gamma <<= 1
		epsilon <<= 1
		if val[1] > val[0] {
			gamma++
		} else {
			epsilon++
		}
	}

	return strconv.Itoa(gamma * epsilon), nil // Return with the answer
}
