// Package day04 2018
package day04

import "gitlab.com/parkermc/advent-of-code/2021/tools"

const testAnswerOne = "65325"
const testAnswerTwo = "4624"

var testFilename = "../test/private/" + tools.GetPackageName()
