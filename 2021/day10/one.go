package day10

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

var scores = map[rune]int{')': 3, ']': 57, '}': 1197, '>': 25137}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	answer := 0
	for _, line := range lines {
		symbolStack := make([]rune, 0)
		for _, r := range line {
			switch r {
			case '(':
				symbolStack = append(symbolStack, ')')
			case '[':
				symbolStack = append(symbolStack, ']')
			case '{':
				symbolStack = append(symbolStack, '}')
			case '<':
				symbolStack = append(symbolStack, '>')
			case '>':
				fallthrough
			case ']':
				fallthrough
			case '}':
				fallthrough
			case ')':
				poped := symbolStack[len(symbolStack)-1]
				symbolStack = symbolStack[:len(symbolStack)-1]
				if poped != r {
					answer += scores[r]
				}
			}
		}
	}

	return strconv.Itoa(answer), nil // Return with the answer
}
