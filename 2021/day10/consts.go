// Package day10 2018
package day10

import "gitlab.com/parkermc/advent-of-code/2021/tools"

const testAnswerOne = "392139"
const testAnswerTwo = "4001832844"

var testFilename = "../test/private/" + tools.GetPackageName()
