// Package day19 2018
package day19

import "gitlab.com/parkermc/advent-of-code/2021/tools"

const testAnswerOne = "419"
const testAnswerTwo = "13210"

var testFilename = "../test/private/" + tools.GetPackageName()
