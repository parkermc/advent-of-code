package day22

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	answer, err := solve(lines, -1)
	if err != nil {
		return "", err
	}
	// 474140

	// expected: 2758514936282235
	// actual:   2758481072261743
	return strconv.FormatUint(answer, 10), nil // Return with the answer
}
