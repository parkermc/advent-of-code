package day22

import (
	"errors"
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

type mode int

const (
	off mode = 1
	on  mode = 2
)

func getMode(str string) (mode, error) {
	switch str {
	case "on":
		return on, nil
	case "off":
		return off, nil
	default:
		return 0, errors.New("Unknown mode: " + str)
	}
}

type probSection struct {
	min        tools.Pos3D
	max        tools.Pos3D
	count      uint64
	exclusions []*probSection
}

func (s *probSection) addExclusions(minPos tools.Pos3D, maxPos tools.Pos3D) uint64 {
	eMin := tools.Pos3D{X: max(s.min.X, minPos.X), Y: max(s.min.Y, minPos.Y), Z: max(s.min.Z, minPos.Z)}
	eMax := tools.Pos3D{X: min(s.max.X, maxPos.X), Y: min(s.max.Y, maxPos.Y), Z: min(s.max.Z, maxPos.Z)}
	size := eMax.Subtract(eMin)

	if size.X <= 0 || size.Y <= 0 || size.Z <= 0 {
		return 0
	}

	var add uint64
	for _, section := range s.exclusions {
		add += section.addExclusions(eMin, eMax)
	}

	overlap := &probSection{
		min:        eMin,
		max:        eMax,
		count:      uint64(size.X) * uint64(size.Y) * uint64(size.Z),
		exclusions: make([]*probSection, 0),
	}
	s.count -= overlap.count
	s.exclusions = append(s.exclusions, overlap)
	s.count += add
	return overlap.count - add
}

func solve(lines []string, capV int) (uint64, error) {
	sections := make([]*probSection, 0)

	for _, line := range lines {
		split := strings.Split(line, " ")
		mode, err := getMode(split[0])
		if err != nil {
			return 0, nil
		}
		cords := strings.Split(split[1], ",")

		minPos := tools.Pos3D{}
		maxPos := tools.Pos3D{}
		// X
		splitX := strings.Split(strings.TrimPrefix(cords[0], "x="), "..")
		minPos.X, err = strconv.Atoi(splitX[0])
		if err != nil {
			return 0, err
		}
		maxPos.X, err = strconv.Atoi(splitX[1])
		if err != nil {
			return 0, err
		}

		// Y
		splitY := strings.Split(strings.TrimPrefix(cords[1], "y="), "..")
		minPos.Y, err = strconv.Atoi(splitY[0])
		if err != nil {
			return 0, err
		}
		maxPos.Y, err = strconv.Atoi(splitY[1])
		if err != nil {
			return 0, err
		}

		// Z
		splitZ := strings.Split(strings.TrimPrefix(cords[2], "z="), "..")
		minPos.Z, err = strconv.Atoi(splitZ[0])
		if err != nil {
			return 0, err
		}
		maxPos.Z, err = strconv.Atoi(splitZ[1])
		if err != nil {
			return 0, err
		}
		if capV > 0 {
			minPos = tools.Pos3D{X: max(-capV, minPos.X), Y: max(-capV, minPos.Y), Z: max(-capV, minPos.Z)}
			maxPos = tools.Pos3D{X: min(capV, maxPos.X), Y: min(capV, maxPos.Y), Z: min(capV, maxPos.Z)}
		}
		maxPos = maxPos.Add(tools.Pos3D{X: 1, Y: 1, Z: 1})
		size := maxPos.Subtract(minPos)
		if size.X < 1 || size.Y < 1 || size.Z < 1 {
			continue
		}

		for _, section := range sections {
			section.addExclusions(minPos, maxPos)
		}

		if mode == on {
			sections = append(sections, &probSection{
				min:        minPos,
				max:        maxPos,
				count:      uint64(size.X) * uint64(size.Y) * uint64(size.Z),
				exclusions: make([]*probSection, 0),
			})
		}
	}

	var total uint64
	for _, section := range sections {
		total += section.count
	}

	return total, nil
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	answer, err := solve(lines, 50)
	if err != nil {
		return "", err
	}
	return strconv.FormatUint(answer, 10), nil // Return with the answer
}
