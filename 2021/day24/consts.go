// Package day24 2021
package day24

import "gitlab.com/parkermc/advent-of-code/2021/tools"

const testAnswerOne = "99919765949498"
const testAnswerTwo = "24913111616151"

var testFilename = "../test/private/" + tools.GetPackageName()
