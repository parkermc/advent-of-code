// Package day02 2018
package day02

import "gitlab.com/parkermc/advent-of-code/2021/tools"

const testAnswerOne = "1427868"
const testAnswerTwo = "1568138742"

var testFilename = "../test/private/" + tools.GetPackageName()
