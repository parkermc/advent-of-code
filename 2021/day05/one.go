package day05

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

func loadData(lines []string, diagnal bool) (map[int]map[int]int, error) {
	var err error
	grid := make(map[int]map[int]int, 0)

	for _, line := range lines {
		split := strings.Split(line, " -> ")
		start := tools.Pos2D{}
		startSplit := strings.Split(split[0], ",")
		start.X, err = strconv.Atoi(startSplit[0])
		if err != nil {
			return nil, err
		}
		start.Y, err = strconv.Atoi(startSplit[1])
		if err != nil {
			return nil, err
		}

		end := tools.Pos2D{}
		endSplit := strings.Split(split[1], ",")
		end.X, err = strconv.Atoi(endSplit[0])
		if err != nil {
			return nil, err
		}
		end.Y, err = strconv.Atoi(endSplit[1])
		if err != nil {
			return nil, err
		}

		if start.X == end.X {
			minV := tools.Min(start.Y, end.Y)
			maxV := tools.Max(start.Y, end.Y)

			for y := minV; y <= maxV; y++ {
				pos := tools.Pos2D{X: end.X, Y: y}
				tools.GridSet(grid, pos, tools.GridGet(grid, pos)+1)
			}
		} else if start.Y == end.Y {
			minV := tools.Min(start.X, end.X)
			maxV := tools.Max(start.X, end.X)

			for x := minV; x <= maxV; x++ {
				pos := tools.Pos2D{X: x, Y: end.Y}
				tools.GridSet(grid, pos, tools.GridGet(grid, pos)+1)
			}
		} else if diagnal {
			minXPos := start
			maxXPos := end
			if maxXPos.X < minXPos.X {
				minXPos, maxXPos = maxXPos, minXPos
			}

			y := minXPos.Y
			for x := minXPos.X; x <= maxXPos.X; x++ {
				pos := tools.Pos2D{X: x, Y: y}
				tools.GridSet(grid, pos, tools.GridGet(grid, pos)+1)
				if maxXPos.Y > minXPos.Y {
					y++
				} else {
					y--

				}
			}
		}
	}

	return grid, nil
}

func getAnswer(grid map[int]map[int]int) int {
	answer := 0
	for _, col := range grid {
		for _, val := range col {
			if val > 1 {
				answer++
			}
		}
	}

	return answer
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	grid, err := loadData(lines, false)
	if err != nil {
		return "", err
	}

	return strconv.Itoa(getAnswer(grid)), nil // Return with the answer
}
