// Package day18 2018
package day18

import "gitlab.com/parkermc/advent-of-code/2021/tools"

const testAnswerOne = "3734"
const testAnswerTwo = "4837"

var testFilename = "../test/private/" + tools.GetPackageName()
