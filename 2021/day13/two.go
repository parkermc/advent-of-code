package day13

import (
	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	grid, err := doFolding(lines, true)
	if err != nil {
		return "", nil
	}

	return tools.GridStr(grid, map[int]string{0: " ", 1: "#"}), nil // Return with the answer
}
