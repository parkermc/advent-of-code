package day17

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

func hitsTarget(targetMin, targetMax, startVol tools.Pos2D) (bool, int) {
	vol := startVol
	pos := tools.Pos2D{X: 0, Y: 0}
	maxY := 0

	for pos.Y >= targetMin.Y && pos.X <= targetMax.X {
		pos = pos.Add(vol)
		if pos.Y > maxY {
			maxY = pos.Y
		}
		if pos.Y <= targetMax.Y && pos.Y >= targetMin.Y && pos.X <= targetMax.X && pos.X >= targetMin.X {
			return true, maxY
		}
		vol.Y--
		if vol.X == 0 {
			continue
		} else if vol.X > 0 {
			vol.X--
		} else {
			vol.X++
		}
	}
	return false, maxY
}

func load(lines []string) (tools.Pos2D, tools.Pos2D, tools.Pos2D, tools.Pos2D, error) {
	split := strings.Split(lines[0][13:], ", ")
	xSplit := strings.Split(split[0][2:], "..")
	xInMin, err := strconv.Atoi(xSplit[0])
	if err != nil {
		return tools.Pos2D{}, tools.Pos2D{}, tools.Pos2D{}, tools.Pos2D{}, err
	}
	xInMax, err := strconv.Atoi(xSplit[1])
	if err != nil {
		return tools.Pos2D{}, tools.Pos2D{}, tools.Pos2D{}, tools.Pos2D{}, err
	}

	ySplit := strings.Split(split[1][2:], "..")
	yInMin, err := strconv.Atoi(ySplit[0])
	if err != nil {
		return tools.Pos2D{}, tools.Pos2D{}, tools.Pos2D{}, tools.Pos2D{}, err
	}
	yInMax, err := strconv.Atoi(ySplit[1])
	if err != nil {
		return tools.Pos2D{}, tools.Pos2D{}, tools.Pos2D{}, tools.Pos2D{}, err
	}
	targetMin := tools.Pos2D{X: xInMin, Y: yInMin}
	targetMax := tools.Pos2D{X: xInMax, Y: yInMax}

	minX := 0
	for res := 0; res < xInMin; minX++ {
		res += minX + 1
	}
	maxX := targetMax.X
	minY := targetMin.Y
	maxY := -targetMin.Y

	return targetMin, targetMax, tools.Pos2D{X: minX, Y: minY}, tools.Pos2D{X: maxX, Y: maxY}, nil
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	targetMin, targetMax, volMin, volMax, err := load(lines)
	if err != nil {
		return "", err
	}

	var mH int
	var hits bool
	maxHeight := 0
	for y := volMax.Y; y >= volMin.Y; y-- {
		hits, mH = hitsTarget(targetMin, targetMax, tools.Pos2D{X: volMin.X, Y: y})
		if hits {
			maxHeight = mH
			break
		}
	}

	return strconv.Itoa(maxHeight), nil // Return with the answer
}
