package day12

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

func countPaths2(paths map[string][]string, cur string, visitedSmall []string, visitedTwice bool) int {
	out := 0
	if cur == "end" {
		return 1
	}
	if !isBigCave(cur) {
		visitedSmall = append(visitedSmall, cur)
	}
	for _, next := range paths[cur] {
		if !isBigCave(next) && alreadyVisted(visitedSmall, next) {
			if visitedTwice || next == "start" {
				continue
			}
			out += countPaths2(paths, next, visitedSmall, true)
			continue
		}
		out += countPaths2(paths, next, visitedSmall, visitedTwice)
	}
	return out

}

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	paths := make(map[string][]string, 0)
	for _, strPath := range lines {
		split := strings.Split(strPath, "-")
		paths[split[0]] = append(paths[split[0]], split[1])
		paths[split[1]] = append(paths[split[1]], split[0])
	}

	return strconv.Itoa(countPaths2(paths, "start", make([]string, 0), false)), nil // Return with the answer
}
