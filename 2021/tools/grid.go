package tools

import (
	"log"
	"strconv"
)

// GridSet sets a spot on the grid
func GridSet(area map[int]map[int]int, pos Pos2D, val int) {
	if _, ok := area[pos.X]; !ok {
		area[pos.X] = map[int]int{pos.Y: val}
	} else {
		area[pos.X][pos.Y] = val
	}
}

// GridGet Get the value at the pos
func GridGet(area map[int]map[int]int, pos Pos2D) int {
	if col, ok := area[pos.X]; ok {
		if val, ok := col[pos.Y]; ok {
			return val
		}
	}
	return 0
}

// GridMinMax returns minX, maxX, minY, maxY
func GridMinMax(area map[int]map[int]int) (minX int, maxX int, minY int, maxY int) {
	minX = 10000000000
	maxX = -10000000000
	minY = 10000000000
	maxY = -10000000000
	for x, line := range area {
		if x < minX {
			minX = x
		}
		if x > maxX {
			maxX = x
		}
		for y := range line {
			if y < minY {
				minY = y
			}
			if y > maxY {
				maxY = y
			}
		}
	}
	return minX, maxX, minY, maxY
}

// GridPrint prints the grid
func GridPrint(area map[int]map[int]int, outMap map[int]string) {
	minX, maxX, minY, maxY := GridMinMax(area)
	for y := minY; y <= maxY; y++ {
		line := ""
		for x := minX; x <= maxX; x++ {
			val := area[x][y]
			if str, ok := outMap[val]; ok {
				line += str
			} else {
				line += strconv.Itoa(val)
			}
		}
		log.Print(line)
	}
}

// GridStr returns and string of the grid
func GridStr(area map[int]map[int]int, outMap map[int]string) string {
	out := ""
	minX, maxX, minY, maxY := GridMinMax(area)
	for y := minY; y <= maxY; y++ {
		for x := minX; x <= maxX; x++ {
			val := area[x][y]
			if str, ok := outMap[val]; ok {
				out += str
			} else {
				out += strconv.Itoa(val)
			}
		}
		out += "\n"
	}
	return out[:len(out)-1]
}
