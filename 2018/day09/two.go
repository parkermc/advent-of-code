package day09

import (
	"strconv"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {
	answer, err := solve(filename, 100)
	return strconv.Itoa(answer), err // Return with the answer
}
