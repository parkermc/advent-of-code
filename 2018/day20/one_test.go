package day20

import "testing"

func TestOne(t *testing.T) {
	answer, err := One(testFilename)
	if err != nil {
		t.Fatal(err)
	}
	if answer != testAnswerOne {
		t.Fatalf("Answer should be \"%s\" but was \"%s\"", testAnswerOne, answer)
	}
}
