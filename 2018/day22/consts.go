// Package day22 2018
package day22

import "gitlab.com/parkermc/advent-of-code/2018/tools"

const testAnswerOne = "10395"
const testAnswerTwo = "1010"

var testFilename = "../test/private/" + tools.GetPackageName()
