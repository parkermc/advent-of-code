// Package day12 2018
package day12

import "gitlab.com/parkermc/advent-of-code/2018/tools"

const testAnswerOne = "2823"
const testAnswerTwo = "2900000001856"

var testFilename = "../test/private/" + tools.GetPackageName()
