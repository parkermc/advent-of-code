// Package day02 2018
package day02

import "gitlab.com/parkermc/advent-of-code/2018/tools"

const testAnswerOne = "5727"
const testAnswerTwo = "uwfmdjxyxlbgnrotcfpvswaqh"

var testFilename = "../test/private/" + tools.GetPackageName()
