// Package day13 2018
package day13

import "gitlab.com/parkermc/advent-of-code/2018/tools"

const testAnswerOne = "71,121"
const testAnswerTwo = "71,76"

var testFilename = "../test/private/" + tools.GetPackageName()
