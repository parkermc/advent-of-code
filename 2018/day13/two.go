package day13

import (
	"errors"
	"sort"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {
	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}
	tracks, carts := createTrack(lines)
	for {
		removeCarts := make([]int, 0)
		for id, cart := range carts {
			if !tracks[cart.X][cart.Y].exits {
				return "", errors.New("No track error")
			}
			trackType := tracks[cart.X][cart.Y].Type
			if trackType == TrackStrait {
				if cart.NX {
					cart.X--
				} else if cart.PX {
					cart.X++
				} else if cart.NY {
					cart.Y--
				} else if cart.PY {
					cart.Y++
				}
			} else if trackType == TrackCornor {
				if (cart.NX || cart.PX) && tracks[cart.X][cart.Y].NY {
					cart.NX = false
					cart.PX = false
					cart.NY = true
					cart.Y--
				} else if (cart.NX || cart.PX) && tracks[cart.X][cart.Y].PY {
					cart.NX = false
					cart.PX = false
					cart.PY = true
					cart.Y++
				} else if (cart.NY || cart.PY) && tracks[cart.X][cart.Y].NX {
					cart.NY = false
					cart.PY = false
					cart.NX = true
					cart.X--
				} else if (cart.NY || cart.PY) && tracks[cart.X][cart.Y].PX {
					cart.NY = false
					cart.PY = false
					cart.PX = true
					cart.X++
				}
			} else if trackType == TrackX {
				if cart.LastTurn == TurnStraight {
					cart.LastTurn = TurnRight
				} else {
					cart.LastTurn++
				}
				if cart.LastTurn == TurnStraight {
					if cart.NX {
						cart.X--
					} else if cart.PX {
						cart.X++
					} else if cart.NY {
						cart.Y--
					} else if cart.PY {
						cart.Y++
					}
				} else if cart.LastTurn == TurnRight {
					if cart.NX {
						cart.Y++
						cart.NX = false
						cart.PY = true
					} else if cart.PX {
						cart.Y--
						cart.PX = false
						cart.NY = true
					} else if cart.NY {
						cart.X--
						cart.NY = false
						cart.NX = true
					} else if cart.PY {
						cart.X++
						cart.PY = false
						cart.PX = true
					}
				} else {
					if cart.PX {
						cart.Y++
						cart.PX = false
						cart.PY = true
					} else if cart.NX {
						cart.Y--
						cart.NX = false
						cart.NY = true
					} else if cart.PY {
						cart.X--
						cart.PY = false
						cart.NX = true
					} else if cart.NY {
						cart.X++
						cart.NY = false
						cart.PX = true
					}
				}
			}
			for id2, cart2 := range carts {
				if id != id2 && cart.X == cart2.X && cart.Y == cart2.Y {
					removeCarts = append(removeCarts, id)
					removeCarts = append(removeCarts, id2)
				}
			}
		}
		sort.Sort(sort.Reverse(sort.IntSlice(removeCarts)))
		for _, id := range removeCarts {
			copy(carts[id:], carts[id+1:])
			carts[len(carts)-1] = nil // or the zero value of T
			carts = carts[:len(carts)-1]
		}
		if len(carts) == 1 {
			break
		}
	}
	return strconv.Itoa(carts[0].Y) + "," + strconv.Itoa(carts[0].X), nil // Return with the answer
}
