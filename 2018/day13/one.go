package day13

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

// TrackType for what kind of track it is
type TrackType int

// TurnType for what turn
type TurnType int

// Set all the things!!!
const (
	TrackNone    TrackType = 0
	TrackStrait  TrackType = 1
	TrackX       TrackType = 2
	TrackCornor  TrackType = 3
	TurnRight    TurnType  = 0
	TurnLeft     TurnType  = 1
	TurnStraight TurnType  = 2
)

// Cart stores the cart
type Cart struct {
	LastTurn TurnType
	X        int
	Y        int
	PX       bool
	PY       bool
	NX       bool
	NY       bool
}

// Track stores each pice of track
type Track struct {
	exits bool
	Type  TrackType
	PX    bool
	PY    bool
	NX    bool
	NY    bool
}

// func draw(tracks [][]Track, carts []*Cart) {
// 	for x, xTrack := range tracks {
// 		out := ""
// 		for y, yTrack := range xTrack {
// 			cartFound := false
// 			for _, cart := range carts {
// 				if cart.X == x && cart.Y == y {
// 					if cart.NY {
// 						out += "<"
// 					} else if cart.PY {
// 						out += ">"
// 					} else if cart.PX {
// 						out += "v"
// 					} else {
// 						out += "^"
// 					}
// 					cartFound = true
// 				}
// 			}
// 			if !cartFound {
// 				if yTrack.Type == TrackCornor {
// 					if (yTrack.PX && yTrack.PY) || (yTrack.NX && yTrack.NY) {
// 						out += "/"
// 					} else {
// 						out += "\\"
// 					}
// 				} else if yTrack.Type == TrackStrait {
// 					if yTrack.NX {
// 						out += "|"
// 					} else {
// 						out += "-"
// 					}
// 				} else if yTrack.Type == TrackX {
// 					out += "+"
// 				} else {
// 					out += " "
// 				}
// 			}
// 		}
// 		log.Print(out)
// 	}
// 	log.Print("\n")
// }

func createTrack(lines []string) ([][]Track, []*Cart) {
	tracks := make([][]Track, len(lines))
	carts := make([]*Cart, 0)
	maxX := 0
	for _, line := range lines {
		if len(line) > maxX {
			maxX = len(line)
		}
	}
	for x, line := range lines {
		tracks[x] = make([]Track, maxX)
		for y, letter := range []rune(line) {
			if letter == '/' || letter == '\\' {
				tracks[x][y].exits = true
				tracks[x][y].Type = TrackCornor
				if letter == '/' {
					if len(lines) > x+1 && len([]rune(lines[x+1])) > y && ([]rune(lines[x+1])[y] == '+' || []rune(lines[x+1])[y] == '|' || []rune(lines[x+1])[y] == 'v' || []rune(lines[x+1])[y] == '^') {
						if len([]rune(lines[x])) > y+1 && ([]rune(lines[x])[y+1] == '+' || []rune(lines[x])[y+1] == '-' || []rune(lines[x])[y+1] == '<' || []rune(lines[x])[y+1] == '>') {
							tracks[x][y].PX = true
							tracks[x][y].PY = true
						}
					}
					if x-1 > -1 && len([]rune(lines[x-1])) > y && ([]rune(lines[x-1])[y] == '+' || []rune(lines[x-1])[y] == '|' || []rune(lines[x-1])[y] == 'v' || []rune(lines[x-1])[y] == '^') {
						if y-1 > -1 && ([]rune(lines[x])[y-1] == '+' || []rune(lines[x])[y-1] == '-' || []rune(lines[x])[y-1] == '<' || []rune(lines[x])[y-1] == '>') {
							tracks[x][y].NY = true
							tracks[x][y].NX = true
						}
					}
				} else {
					if len(lines) > x+1 && len([]rune(lines[x+1])) > y && ([]rune(lines[x+1])[y] == '+' || []rune(lines[x+1])[y] == '|' || []rune(lines[x+1])[y] == 'v' || []rune(lines[x+1])[y] == '^') {
						if y-1 > -1 && ([]rune(lines[x])[y-1] == '+' || []rune(lines[x])[y-1] == '-' || []rune(lines[x])[y-1] == '<' || []rune(lines[x])[y-1] == '>') {
							tracks[x][y].NY = true
							tracks[x][y].PX = true
						}
					}
					if x-1 > -1 && len([]rune(lines[x-1])) > y && ([]rune(lines[x-1])[y] == '+' || []rune(lines[x-1])[y] == '|' || []rune(lines[x-1])[y] == 'v' || []rune(lines[x-1])[y] == '^') {
						if len([]rune(lines[x])) > y+1 && ([]rune(lines[x])[y+1] == '+' || []rune(lines[x])[y+1] == '-' || []rune(lines[x])[y+1] == '<' || []rune(lines[x])[y+1] == '>') {
							tracks[x][y].NX = true
							tracks[x][y].PY = true
						}
					}
				}

			} else if letter == '-' || letter == '|' || letter == '<' || letter == '>' || letter == 'v' || letter == '^' { // TODO add cart
				if letter == '<' || letter == '>' || letter == 'v' || letter == '^' {
					if letter == '<' {
						carts = append(carts, &Cart{TurnRight, x, y, false, false, false, true})
					} else if letter == '>' {
						carts = append(carts, &Cart{TurnRight, x, y, false, true, false, false})
					} else if letter == 'v' {
						carts = append(carts, &Cart{TurnRight, x, y, true, false, false, false})
					} else {
						carts = append(carts, &Cart{TurnRight, x, y, false, false, true, false})
					}
				}
				tracks[x][y].exits = true
				tracks[x][y].Type = TrackStrait
				if y-1 > -1 && len([]rune(lines[x])) > y+1 && ([]rune(lines[x])[y-1] == '+' || []rune(lines[x])[y-1] == '-' || []rune(lines[x])[y-1] == '<' || []rune(lines[x])[y-1] == '>' || []rune(lines[x])[y-1] == '\\' || []rune(lines[x])[y-1] == '/') &&
					([]rune(lines[x])[y+1] == '+' || []rune(lines[x])[y+1] == '-' || []rune(lines[x])[y+1] == '<' || []rune(lines[x])[y+1] == '>' || []rune(lines[x])[y+1] == '\\' || []rune(lines[x])[y+1] == '/') {
					tracks[x][y].NY = true
					tracks[x][y].PY = true
				} else {
					tracks[x][y].NX = true
					tracks[x][y].PX = true
				}
			} else if letter == '+' {
				tracks[x][y].exits = true
				tracks[x][y].Type = TrackX
				tracks[x][y].NY = true
				tracks[x][y].PY = true
				tracks[x][y].NX = true
				tracks[x][y].PX = true
			}
		}
	}
	return tracks, carts
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	tracks, carts := createTrack(lines)

	for {
		for id, cart := range carts {
			trackType := tracks[cart.X][cart.Y].Type
			if trackType == TrackStrait {
				if cart.NX {
					cart.X--
				} else if cart.PX {
					cart.X++
				} else if cart.NY {
					cart.Y--
				} else if cart.PY {
					cart.Y++
				}
			} else if trackType == TrackCornor {
				if (cart.NX || cart.PX) && tracks[cart.X][cart.Y].NY {
					cart.NX = false
					cart.PX = false
					cart.NY = true
					cart.Y--
				} else if (cart.NX || cart.PX) && tracks[cart.X][cart.Y].PY {
					cart.NX = false
					cart.PX = false
					cart.PY = true
					cart.Y++
				} else if (cart.NY || cart.PY) && tracks[cart.X][cart.Y].NX {
					cart.NY = false
					cart.PY = false
					cart.NX = true
					cart.X--
				} else if (cart.NY || cart.PY) && tracks[cart.X][cart.Y].PX {
					cart.NY = false
					cart.PY = false
					cart.PX = true
					cart.X++
				}
			} else if trackType == TrackX {
				if cart.LastTurn == TurnStraight {
					cart.LastTurn = TurnRight
				} else {
					cart.LastTurn++
				}
				if cart.LastTurn == TurnStraight {
					if cart.NX {
						cart.X--
					} else if cart.PX {
						cart.X++
					} else if cart.NY {
						cart.Y--
					} else if cart.PY {
						cart.Y++
					}
				} else if cart.LastTurn == TurnRight {
					if cart.NX {
						cart.Y++
						cart.NX = false
						cart.PY = true
					} else if cart.PX {
						cart.Y--
						cart.PX = false
						cart.NY = true
					} else if cart.NY {
						cart.X--
						cart.NY = false
						cart.NX = true
					} else if cart.PY {
						cart.X++
						cart.PY = false
						cart.PX = true
					}
				} else {
					if cart.PX {
						cart.Y++
						cart.PX = false
						cart.PY = true
					} else if cart.NX {
						cart.Y--
						cart.NX = false
						cart.NY = true
					} else if cart.PY {
						cart.X--
						cart.PY = false
						cart.NX = true
					} else if cart.NY {
						cart.X++
						cart.NY = false
						cart.PX = true
					}
				}
			}
			for id2, cart2 := range carts {
				if id != id2 && cart.X == cart2.X && cart.Y == cart2.Y {
					return strconv.Itoa(cart.Y) + "," + strconv.Itoa(cart.X), nil
				}
			}
		}
	}
}
