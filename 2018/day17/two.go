package day17

import "strconv"

// Two the second puzzle for the day
func Two(filename string) (string, error) {
	grid, err := getGrid(filename)
	if err != nil {
		return "", nil
	}
	answer := 0
	for _, xWall := range grid {
		for _, block := range xWall {
			if block.Type == RestWater {
				answer++
			}
		}
	}
	return strconv.Itoa(answer), nil // Return with the answer
}
