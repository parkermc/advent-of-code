package day21

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {
	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}
	nums := make([][]int, len(lines)-1)
	for i, line := range lines[1:] {
		split := strings.Split(line[5:], " ")
		nums[i] = make([]int, len(split))
		for j, s := range split {
			nums[i][j], err = strconv.Atoi(s)
			if err != nil {
				return "", err
			}
		}
	}

	// Reverse engineered with help from online
	b := 0
	a := 0
	c := 0
	seen := make(map[int]bool)
	last := 0
	for {
		b = c | nums[6][1]
		c = nums[7][0]
		for {
			a = b & nums[8][1]
			c += a
			c &= nums[10][1]
			c *= nums[11][1]
			c &= nums[12][1]
			if nums[13][0] > b {
				if seen[c] {
					return strconv.Itoa(last), nil
				}
				last = c
				seen[c] = true
				break
			}
			b = b / nums[19][1]
		}
	}
}
