// Package day11 2018
package day11

import "gitlab.com/parkermc/advent-of-code/2018/tools"

const testAnswerOne = "20,51"
const testAnswerTwo = "230,272,17"

var testFilename = "../test/private/" + tools.GetPackageName()
