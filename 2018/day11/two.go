package day11

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}
	serial, err := strconv.Atoi(lines[0])
	if err != nil {
		return "", err
	}

	grid := make([][]int, 0)
	for x := 0; x < 300; x++ {
		grid = append(grid, make([]int, 0))
		for y := 0; y < 300; y++ {
			rackID := x + 10
			power := (((rackID * y) + serial) * rackID)
			power = ((power / 100) % 10) - 5
			grid[x] = append(grid[x], power)
		}
	}
	lastGrid := make([][]int, 0)
	for x := 0; x < 300; x++ {
		lastGrid = append(lastGrid, make([]int, 300))
	}
	mostX := -1
	mostY := -1
	mostSize := -1
	mostAmt := -9999999999999999
	for size := 0; size < 300; size++ {
		for x := 0; x < 300-size; x++ {
			for y := 0; y < 300-size; y++ {
				lastGrid[x][y] += grid[x+size][y+size]
				for i := 0; i < size; i++ {
					lastGrid[x][y] += grid[x+i][y+size]
					lastGrid[x][y] += grid[x+size][y+i]
				}
				if lastGrid[x][y] >= mostAmt {
					mostX = x
					mostY = y
					mostAmt = lastGrid[x][y]
					mostSize = size + 1
				}
			}
		}
	}
	return strconv.Itoa(mostX) + "," + strconv.Itoa(mostY) + "," + strconv.Itoa(mostSize), nil // Return with the answer
}
