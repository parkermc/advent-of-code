package day18

import (
	"errors"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

// AcreType use
type AcreType int

// Set the types
const (
	Open       AcreType = 0
	Trees      AcreType = 1
	Lumberyard AcreType = 2
)

func run(filename string, times int) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	grid := make([][]AcreType, 0)
	for x, line := range lines {
		grid = append(grid, make([]AcreType, 0))
		for _, letter := range line {
			if letter == '.' {
				grid[x] = append(grid[x], Open)
			} else if letter == '|' {
				grid[x] = append(grid[x], Trees)
			} else if letter == '#' {
				grid[x] = append(grid[x], Lumberyard)
			} else {
				return "", errors.New("data error")
			}
		}
	}

	pastGrids := make([][][]AcreType, 0)
	for minV := 1; minV <= times; minV++ {
		newGrid := make([][]AcreType, len(grid))
		copy(newGrid, grid)
		pastGrids = append(pastGrids, newGrid)

		buf := nextStage(grid)
		grid = buf

		for i, gridTest := range pastGrids {
			same := true
			for x, rowTest := range gridTest {
				for y, acreTest := range rowTest {
					if grid[x][y] != acreTest {
						same = false
						break
					}
				}
				if !same {
					break
				}
			}
			if same {
				more := (times - minV) % (minV - i)
				for min2 := 1; min2 <= more; min2++ {
					buf := nextStage(grid)
					grid = buf
				}

				return getAnswer(grid), nil // Return with the answer
			}
		}
	}

	return getAnswer(grid), nil // Return with the answer
}

func nextStage(grid [][]AcreType) [][]AcreType {
	buf := make([][]AcreType, 0)
	for x, xWall := range grid {
		buf = append(buf, make([]AcreType, 0))
		for y, acre := range xWall {
			woodAmt := 0
			lumberAmt := 0
			for _, pos := range [][]int{[]int{x - 1, y - 1}, []int{x, y - 1}, []int{x + 1, y - 1}, []int{x - 1, y}, []int{x + 1, y}, []int{x - 1, y + 1}, []int{x, y + 1}, []int{x + 1, y + 1}} {
				if pos[0] > -1 && pos[1] > -1 && pos[0] < len(grid) && pos[1] < len(grid[pos[0]]) {
					if grid[pos[0]][pos[1]] == Trees {
						woodAmt++
					} else if grid[pos[0]][pos[1]] == Lumberyard {
						lumberAmt++
					}
				}
			}
			if acre == Open && woodAmt >= 3 {
				buf[x] = append(buf[x], Trees)
			} else if acre == Trees && lumberAmt >= 3 {
				buf[x] = append(buf[x], Lumberyard)
			} else if acre == Lumberyard && (lumberAmt < 1 || woodAmt < 1) {
				buf[x] = append(buf[x], Open)
			} else {
				buf[x] = append(buf[x], acre)
			}
		}
	}
	return buf
}

func getAnswer(grid [][]AcreType) string {
	woodAmt := 0
	lumberAmt := 0
	for _, wallX := range grid {
		for _, acre := range wallX {
			if acre == Trees {
				woodAmt++
			} else if acre == Lumberyard {
				lumberAmt++
			}
		}
	}
	return strconv.Itoa(woodAmt * lumberAmt)
}

// One the first puzzle for the day
func One(filename string) (string, error) {
	return run(filename, 10)
}
