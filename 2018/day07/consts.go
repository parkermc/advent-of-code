// Package day07 2018
package day07

import "gitlab.com/parkermc/advent-of-code/2018/tools"

const testAnswerOne = "CFGHAEMNBPRDISVWQUZJYTKLOX"
const testAnswerTwo = "828"

var testFilename = "../test/private/" + tools.GetPackageName()
