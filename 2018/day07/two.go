package day07

import (
	"sort"
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

// Worker stores the worker person
type Worker struct {
	TimeSpent int
	Job       string
}

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}
	answer := make([]string, 0)
	todo := make([]string, 0)
	steps := make(map[string]*Step, 0)
	for _, id := range "QWERTYUIOPASDFGHJKLZXCVBNM" {
		steps[string(id)] = &Step{string(id), make([]string, 0), make([]string, 0), int(id) - 4, false}
		todo = append(todo, string(id))
	}

	for _, line := range lines {
		steps[strings.Split(line, " ")[7]].Pre = append(steps[strings.Split(line, " ")[7]].Pre, strings.Split(line, " ")[1])
	}
	ticks := -2
	workers := make([]*Worker, 5)
	for i := 0; i < len(workers); i++ {
		workers[i] = &Worker{0, ""}
	}
	for len(todo) > 0 {
		ticks++
		for _, worker := range workers {
			worker.TimeSpent++
			if worker.Job != "" && steps[worker.Job].Time == worker.TimeSpent {
				answer = append(answer, worker.Job)
				for i, letter := range todo {
					if letter == worker.Job {
						todo = append(todo[:i], todo[i+1:]...)
					}
				}
				worker.Job = ""
			}
			if worker.Job == "" {
				canDo := make([]string, 0)
				for _, step := range steps {
					if (!step.Assigned) && step.CanDo(answer) {
						canDo = append(canDo, step.ID)
					}
				}
				if len(canDo) > 0 {
					sort.Strings(canDo)
					steps[canDo[0]].Assigned = true
					worker.TimeSpent = 0
					worker.Job = canDo[0]
				}
			}
		}
	}

	return strconv.Itoa(ticks), nil // Return with the answer
}
