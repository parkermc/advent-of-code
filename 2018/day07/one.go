package day07

import (
	"sort"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

// Step a step in the process
type Step struct {
	ID       string
	Pre      []string
	Post     []string
	Time     int
	Assigned bool
}

// CanDo checks if the task can be done
func (step *Step) CanDo(answer []string) bool {
	for _, letterDone := range answer {
		if letterDone == step.ID {
			return false
		}
	}
	can := true
	for _, pre := range step.Pre {
		can = false
		for _, letterDone := range answer {
			if letterDone == pre {
				can = true
				break
			}
		}
		if !can {
			break
		}
	}
	return can
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}
	answer := make([]string, 0)
	todo := make([]string, 0)
	steps := make(map[string]*Step, 0)

	for _, id := range "QWERTYUIOPASDFGHJKLZXCVBNM" {
		steps[string(id)] = &Step{string(id), make([]string, 0), make([]string, 0), -1, false} // Time is un needed
		todo = append(todo, string(id))
	}

	for _, line := range lines {
		steps[strings.Split(line, " ")[7]].Pre = append(steps[strings.Split(line, " ")[7]].Pre, strings.Split(line, " ")[1])
	}

	for len(todo) > 0 {
		canDo := make([]string, 0)
		for _, step := range steps {
			if step.CanDo(answer) {
				canDo = append(canDo, step.ID)
			}
		}
		sort.Strings(canDo)
		answer = append(answer, canDo[0])
		for i, letter := range todo {
			if letter == canDo[0] {
				todo = append(todo[:i], todo[i+1:]...)
			}
		}
	}

	return strings.Join(answer, ""), nil // Return with the answer
}
