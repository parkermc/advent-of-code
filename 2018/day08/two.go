package day08

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

func twoRec(data []string, start int) (indexUsed int, answer int, err error) {
	answer = 0
	indexUsed = 2
	childrenAnswers := make([]int, 0)
	childrenCount, err := strconv.Atoi(data[start])
	if err != nil {
		return 0, 0, err
	}
	metadataCount, err := strconv.Atoi(data[start+1])
	if err != nil {
		return 0, 0, err
	}
	for i := 0; i < childrenCount; i++ {
		used, add, err := twoRec(data, start+indexUsed)
		if err != nil {
			return 0, 0, err
		}
		childrenAnswers = append(childrenAnswers, add)
		indexUsed += used
	}

	if childrenCount == 0 {
		for i := 0; i < metadataCount; i++ {
			add, err := strconv.Atoi(data[start+indexUsed])
			if err != nil {
				return 0, 0, nil
			}
			answer += add
			indexUsed++
		}
	} else {
		for i := 0; i < metadataCount; i++ {
			meta, err := strconv.Atoi(data[start+indexUsed])
			if err != nil {
				return 0, 0, nil
			}
			if meta > 0 && len(childrenAnswers) > meta-1 {
				answer += childrenAnswers[meta-1]
			}
			indexUsed++
		}
	}
	return indexUsed, answer, nil
}

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}
	data := strings.Split(lines[0], " ")
	_, answer, err := twoRec(data, 0)
	if err != nil {
		return "", err
	}
	return strconv.Itoa(answer), nil // Return with the answer
}
