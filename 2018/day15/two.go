package day15

import (
	"errors"
	"sort"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}
	unitsOrg := make([]*Unit, 0)
	walls := make([][]bool, 0)
	for x, line := range lines {
		walls = append(walls, make([]bool, 0))
		for y, char := range []rune(line) {
			if char == '#' {
				walls[x] = append(walls[x], true)
			} else {
				if char == 'E' {
					unitsOrg = append(unitsOrg, &Unit{x, y, 200, UnitElf, true})
				} else if char == 'G' {
					unitsOrg = append(unitsOrg, &Unit{x, y, 200, UnitGoblin, true})
				}
				walls[x] = append(walls[x], false)
			}
		}
	}

	for elfAttack := 3; true; elfAttack++ {
		//draw(walls, units)
		units := make([]*Unit, 0)
		for _, unit := range unitsOrg {
			units = append(units, &Unit{unit.X, unit.Y, unit.HP, unit.Type, unit.Alive})
		}
		nextTick := true
		var tick int
		for tick = 0; nextTick; tick++ {
			sort.SliceStable(units, func(i int, j int) bool {
				if units[i].X < units[j].X || (units[i].X == units[j].X && units[i].Y < units[j].Y) {
					return true
				}
				return false
			})
			//log.Printf("Round %d:\n\n", tick+1)
			for _, unit := range units {
				if unit.Alive {
					unit.Move(units, walls, elfAttack)
				}
			}
			//draw(walls, units)
			nextTick = true
			for _, unit := range units {
				if unit.Type == UnitElf && !unit.Alive {
					nextTick = false
					break
				}
			}
			if nextTick {
				nextTick = false
				for _, unit := range units {
					if unit.Type == UnitGoblin && unit.Alive {
						nextTick = true
						break
					}
				}
			}
		}
		good := true
		for _, unit := range units {
			if unit.Type == UnitElf && !unit.Alive {
				good = false
				break
			}
		}
		if good {
			answer := 0
			for _, unit := range units {
				if unit.Alive {
					answer += unit.HP
				}
			}

			answer *= tick - 1
			return strconv.Itoa(answer), nil // Return with the answer
		}
	}
	return "", errors.New("this should never been seen")
}
