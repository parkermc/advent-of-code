// Main package for 2018
package main

import (
	"log"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2018/day01"
	"gitlab.com/parkermc/advent-of-code/2018/day02"
	"gitlab.com/parkermc/advent-of-code/2018/day03"
	"gitlab.com/parkermc/advent-of-code/2018/day04"
	"gitlab.com/parkermc/advent-of-code/2018/day05"
	"gitlab.com/parkermc/advent-of-code/2018/day06"
	"gitlab.com/parkermc/advent-of-code/2018/day07"
	"gitlab.com/parkermc/advent-of-code/2018/day08"
	"gitlab.com/parkermc/advent-of-code/2018/day09"
	"gitlab.com/parkermc/advent-of-code/2018/day10"
	"gitlab.com/parkermc/advent-of-code/2018/day11"
	"gitlab.com/parkermc/advent-of-code/2018/day12"
	"gitlab.com/parkermc/advent-of-code/2018/day13"
	"gitlab.com/parkermc/advent-of-code/2018/day14"
	"gitlab.com/parkermc/advent-of-code/2018/day15"
	"gitlab.com/parkermc/advent-of-code/2018/day16"
	"gitlab.com/parkermc/advent-of-code/2018/day17"
	"gitlab.com/parkermc/advent-of-code/2018/day18"
	"gitlab.com/parkermc/advent-of-code/2018/day19"
	"gitlab.com/parkermc/advent-of-code/2018/day20"
	"gitlab.com/parkermc/advent-of-code/2018/day21"
	"gitlab.com/parkermc/advent-of-code/2018/day22"
	"gitlab.com/parkermc/advent-of-code/2018/day23"
	"gitlab.com/parkermc/advent-of-code/2018/day24"
	"gitlab.com/parkermc/advent-of-code/2018/day25"
)

const showAll = false

// Puzzle used for the puzzle array
type Puzzle func(string) (string, error)

var puzzles = [][]Puzzle{
	{day01.One, day01.Two},
	{day02.One, day02.Two},
	{day03.One, day03.Two},
	{day04.One, day04.Two},
	{day05.One, day05.Two},
	{day06.One, day06.Two},
	{day07.One, day07.Two},
	{day08.One, day08.Two},
	{day09.One, day09.Two},
	{day10.One, day10.Two},
	{day11.One, day11.Two},
	{day12.One, day12.Two},
	{day13.One, day13.Two},
	{day14.One, day14.Two},
	{day15.One, day15.Two},
	{day16.One, day16.Two},
	{day17.One, day17.Two},
	{day18.One, day18.Two},
	{day19.One, day19.Two},
	{day20.One, day20.Two},
	{day21.One, day21.Two},
	{day22.One, day22.Two},
	{day23.One, day23.Two},
	{day24.One, day24.Two},
	{day25.One, day25.Two},
}

func main() {
	if showAll {
		for dayNum, dayPuzzles := range puzzles {
			for puzzleNum := range dayPuzzles {
				runPuzzle(dayNum+1, puzzleNum+1)
			}
		}
	} else {
		dayNum := len(puzzles)
		puzzleNum := len(puzzles[dayNum-1])
		runPuzzle(dayNum, puzzleNum)
	}
}

func runPuzzle(dayNum int, puzzleNum int) {
	inputFilename := "input"
	if dayNum < 10 {
		inputFilename = "day0" + strconv.Itoa(dayNum) + "/" + inputFilename
	} else {
		inputFilename = "day" + strconv.Itoa(dayNum) + "/" + inputFilename
	}
	answer, err := puzzles[dayNum-1][puzzleNum-1](inputFilename)
	if err != nil {
		log.Printf("Error with day %d puzzle %d: %s", dayNum, puzzleNum, err)
	}
	log.Printf("Day %d puzzle %d: %s", dayNum, puzzleNum, answer)
}
