package day03

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

// Two the first puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}
	claims := make([]Claim, 0)
	for _, line := range lines {
		parts := strings.Split(line, " ")
		pos := strings.Split(parts[2], ",")
		size := strings.Split(parts[3], "x")
		x, err := strconv.Atoi(pos[0])
		if err != nil {
			return "", err
		}
		y, err := strconv.Atoi(strings.Split(pos[1], ":")[0])
		if err != nil {
			return "", err
		}
		width, err := strconv.Atoi(size[0])
		if err != nil {
			return "", err
		}
		height, err := strconv.Atoi(size[1])
		if err != nil {
			return "", err
		}
		claims = append(claims, Claim{x, y, width, height})
	}
	table := make([][]int, 0)
	for x := 0; x < 1000; x++ {
		table = append(table, make([]int, 0))
		for y := 0; y < 1000; y++ {
			table[x] = append(table[x], 0)
		}
	}
	for _, claim := range claims {
		for w := 0; w < claim.width; w++ {
			for h := 0; h < claim.height; h++ {
				table[claim.x+w][claim.y+h]++
			}
		}
	}
	id := 0
	for i, claim := range claims {
		overlap := false
		for w := 0; w < claim.width; w++ {
			for h := 0; h < claim.height; h++ {
				if table[claim.x+w][claim.y+h] > 1 {
					overlap = true
					break
				}
			}
			if overlap {
				break
			}
		}
		if !overlap {
			id = i + 1
		}
	}
	return strconv.Itoa(id), nil // Return with the answer
}
