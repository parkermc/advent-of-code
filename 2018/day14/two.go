package day14

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}
	recipes := make([]int, 0)
	recipes = append(recipes, 3, 7)
	elfOne := 0
	elfTwo := 1
	prevLastI := 1

	for recipeCount := 0; true; recipeCount++ {
		for _, recipeChar := range strconv.Itoa(recipes[elfOne] + recipes[elfTwo]) {
			recipe, err := strconv.Atoi(string(recipeChar))
			if err != nil {
				return "", err
			}
			recipes = append(recipes, recipe)
		}
		elfOne += 1 + recipes[elfOne]
		if elfOne >= len(recipes) {
			elfOne %= len(recipes)
		}
		elfTwo += 1 + recipes[elfTwo]
		if elfTwo >= len(recipes) {
			elfTwo %= len(recipes)
		}
		for ; prevLastI < len(recipes); prevLastI++ {
			found := true
			for i, char := range []rune(lines[0]) {
				if prevLastI-(len(lines[0])-i-1) < 0 || string(char) != strconv.Itoa(recipes[prevLastI-(len(lines[0])-i-1)]) {
					found = false
					break
				}
			}
			if found {
				return strconv.Itoa(1 + len(recipes[:prevLastI]) - len(lines[0])), nil
			}
		}
	}
	return "", nil // Return with the answer
}
