package day06

import (
	"errors"
	"math"
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

// Two the Second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}
	points := make([]Point, 0)
	for _, line := range lines {
		lineSplit := strings.Split(line, ", ")
		x, err := strconv.Atoi(lineSplit[0])
		if err != nil {
			return "", err
		}
		z, err := strconv.Atoi(lineSplit[1])
		if err != nil {
			return "", err
		}
		if x < 0 || z < 0 {
			return "", errors.New("program does not support negative cords")
		}
		points = append(points, Point{x, z, false, 0})
	}

	biggestX := points[0].X
	biggestZ := points[0].Z
	for _, point := range points {
		if biggestX < point.X {
			biggestX = point.X
		}
		if biggestZ < point.Z {
			biggestZ = point.Z
		}
	}

	underTotal := 0
	for x := 0; x <= biggestX; x++ {
		for z := 0; z <= biggestZ; z++ {
			total := 0
			for _, point := range points {
				total += int(math.Abs(float64(point.X-x))) + int(math.Abs(float64(point.Z-z)))
			}
			if total < 10000 {
				underTotal++
			}
		}
	}

	return strconv.Itoa(underTotal), nil // Return with the answer
}
