// Package day16 2018
package day16

import "gitlab.com/parkermc/advent-of-code/2018/tools"

const testAnswerOne = "618"
const testAnswerTwo = "514"

var testFilename = "../test/private/" + tools.GetPackageName()
