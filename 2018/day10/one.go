package day10

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

// Point stores the point
type Point struct {
	x    int
	y    int
	volX int
	volY int
}

func solver(filename string) (answer string, secondAnswer string, err error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", "", err
	}
	points := make([]Point, 0)
	for _, line := range lines {
		x, err := strconv.Atoi(strings.Replace(strings.Split(strings.Split(line, "<")[1], ",")[0], " ", "", -1))
		if err != nil {
			return "", "", err
		}
		y, err := strconv.Atoi(strings.Replace(strings.Split(strings.Split(line, ",")[1], ">")[0], " ", "", -1))
		if err != nil {
			return "", "", err
		}
		volX, err := strconv.Atoi(strings.Replace(strings.Split(strings.Split(line, "<")[2], ",")[0], " ", "", -1))
		if err != nil {
			return "", "", err
		}
		volY, err := strconv.Atoi(strings.Replace(strings.Split(strings.Split(line, ",")[2], ">")[0], " ", "", -1))
		if err != nil {
			return "", "", err
		}
		points = append(points, Point{y, x, volY, volX})
	}
	lastSizeX := 1000000000000000000
	lastSizeY := 1000000000000000000
	var second int
	var smallestX int
	var smallestY int
	for second = 0; true; second++ {
		smallestX = 1000000000000000000
		smallestY = 1000000000000000000
		biggestX := 0
		biggestY := 0
		for _, point := range points {
			if point.x+(point.volX*second) > biggestX {
				biggestX = point.x + (point.volX * second)
			}
			if point.y+(point.volY*second) > biggestY {
				biggestY = point.y + (point.volY * second)
			}
			if point.x+(point.volX*second) < smallestX {
				smallestX = point.x + (point.volX * second)
			}
			if point.y+(point.volY*second) < smallestY {
				smallestY = point.y + (point.volY * second)
			}
		}
		if lastSizeX < biggestX-smallestX || lastSizeY < biggestY-smallestY {
			break
		}
		lastSizeX = biggestX - smallestX
		lastSizeY = biggestY - smallestY
	}
	answer = ""
	for x := 0; x <= lastSizeX; x++ {
		answer += "\n"
		for y := 0; y <= lastSizeY; y++ {
			isPoint := false
			for _, point := range points {
				if (point.x+(point.volX*(second-1))) == x+smallestX+5 && (point.y+(point.volY*(second-1))) == y+smallestY+5 {
					isPoint = true
					break
				}
			}
			if isPoint {
				answer += "#"
			} else {
				answer += " "
			}
		}
	}
	return answer, strconv.Itoa(second - 1), nil
}

// One the first puzzle for the day
func One(filename string) (string, error) {
	answer, _, err := solver(filename)
	return answer, err // Return with the answer
}
