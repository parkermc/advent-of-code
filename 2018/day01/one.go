package day01

import (
	"fmt"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	frequency := 0
	for _, line := range lines { // Loop though the lines
		number, err := strconv.Atoi(string([]rune(line)[1:])) // Get the number
		if err != nil {
			return "", err
		}
		sign := []rune(line)[0] // Get the sign

		if sign == '-' { // If the sign is negative remove the number from the frequency
			frequency -= number
		} else if sign == '+' { // If the sign is positive add the number from the frequency
			frequency += number
		} else { // Else there is some error with the input data
			return "", fmt.Errorf("\"%s\" is not \"+\" or \"-\"", string([]rune{sign}))
		}
	}

	return strconv.Itoa(frequency), nil // Return with the answer
}
