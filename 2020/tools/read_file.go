package tools

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

// ReadFile Reads a file to string
func ReadFile(filename string) (string, error) {
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		return "", fmt.Errorf("please create \"%s\" with input data it does not exist", filename)
	}
	text, err := ioutil.ReadFile(filename)
	if err != nil {
		return "", err
	}
	return string(text), nil
}

// ReadFileLines reads the file lines to an array
func ReadFileLines(filename string) ([]string, error) {
	text, err := ReadFile(filename)
	if err != nil {
		return []string{}, err
	}
	return strings.Split(strings.ReplaceAll((text), "\r", ""), "\n"), nil
}

// ReadFileLinesRemoveLast reads the file lines to an array and removes the last line if it's blank
func ReadFileLinesRemoveLast(filename string) ([]string, error) {
	lines, err := ReadFileLines(filename)
	if err != nil {
		return []string{}, err
	}
	if lines[len(lines)-1] == "" {
		lines = lines[:len(lines)-1]
	}
	return lines, nil
}

// ReadFileLinesRequireLast reads the file lines to an array and adds a last line if it's not there
func ReadFileLinesRequireLast(filename string) ([]string, error) {
	lines, err := ReadFileLines(filename)
	if err != nil {
		return []string{}, err
	}
	if lines[len(lines)-1] != "" {
		lines = append(lines, "")
	}
	return lines, nil
}
