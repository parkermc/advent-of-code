package tools

// TrimFirstChar trims the first char from the string
func TrimFirstChar(s string) (string, string) {
	for i := range s {
		if i > 0 {
			// The value i is the index in s of the second
			// rune.  Slice to remove the first rune.
			return s[:i], s[i:]
		}
	}

	// There are 0 or 1 runes in the string.
	if len(s) == 1 {
		return s, ""
	}
	return "", ""

}
