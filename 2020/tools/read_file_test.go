package tools

import (
	"strings"
	"testing"
)

const testFilename = "test_read"
const testFilenameNoNewline = "test_read_no_newline"
const testNoFilename = "test_no_read"

const testReadFileAnswer = "testing\nthe\nreading\nfile\nthingy\n"

var testReadFileLinesAnswer = []string{"testing", "the", "reading", "file", "thingy", ""}
var testReadFileLinesRemoveLastAnswer = []string{"testing", "the", "reading", "file", "thingy"}
var testReadFileLinesRequireLastAnswer = []string{"testing", "the", "reading", "file", "thingy", ""}

func TestReadFile(t *testing.T) {
	text, err := ReadFile(testFilename)
	if err != nil {
		t.Fatal(err)
	}

	if strings.ReplaceAll(text, "\r", "") != testReadFileAnswer {
		t.Fatal("the text read is incorrect")
	}

	_, err = ReadFile(testNoFilename)
	if err == nil {
		t.Fatal("read file that shouldn't exist")
	}
}

func TestReadFileLines(t *testing.T) {
	lines, _ := ReadFileLines(testFilename)

	for i, line := range lines {
		if len(testReadFileLinesAnswer) <= i || line != testReadFileLinesAnswer[i] {
			t.Fatal("the text read is incorrect")
		}
	}
}

func TestReadFileLinesRemoveLast(t *testing.T) {
	lines, _ := ReadFileLinesRemoveLast(testFilename)

	for i, line := range lines {
		if len(testReadFileLinesRemoveLastAnswer) <= i || line != testReadFileLinesRemoveLastAnswer[i] {
			t.Fatal("the text read is incorrect")
		}
	}
}

func TestReadFileLinesRequireLast(t *testing.T) {
	lines, _ := ReadFileLinesRequireLast(testFilename)

	for i, line := range lines {
		if len(testReadFileLinesRequireLastAnswer) <= i || line != testReadFileLinesRequireLastAnswer[i] {
			t.Fatal("the text read is incorrect")
		}
	}

	lines, _ = ReadFileLinesRequireLast(testFilenameNoNewline)
	for i, line := range lines {
		if len(testReadFileLinesRequireLastAnswer) <= i || line != testReadFileLinesRequireLastAnswer[i] {
			t.Fatal("the text read is incorrect")
		}
	}
}
