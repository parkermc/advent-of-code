// Package day18 2020
package day18

import "gitlab.com/parkermc/advent-of-code/2020/tools"

const testAnswerOne = "21993583522852"
const testAnswerTwo = "122438593522757"

var testFilename = "../test/private/" + tools.GetPackageName()
