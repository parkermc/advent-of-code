// Package day02 2020
package day02

import "gitlab.com/parkermc/advent-of-code/2020/tools"

const testAnswerOne = "622"
const testAnswerTwo = "263"

var testFilename = "../test/private/" + tools.GetPackageName()
