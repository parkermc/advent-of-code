package day24

import (
	"fmt"
	"math"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

func load(lines []string) (map[tools.Pos2D]bool, tools.Pos2D, tools.Pos2D, error) {
	isBlack := make(map[tools.Pos2D]bool, len(lines)) // Set legnth too high because I'm not worried about mem usage
	min := tools.Pos2D{X: math.MaxInt64, Y: math.MaxInt64}
	max := tools.Pos2D{X: -math.MaxInt64, Y: -math.MaxInt64}
	for _, line := range lines {
		pos := tools.Pos2D{X: 0, Y: 0}
		for i := 0; i < len(line); i++ {
			switch line[i] {
			case 'e':
				pos.X += 2
			case 'w':
				pos.X -= 2
			case 's':
				pos.Y--
				i++
				switch line[i] {
				case 'e':
					pos.X++
				case 'w':
					pos.X--
				default:
					return nil, tools.Pos2D{}, tools.Pos2D{}, fmt.Errorf("no clue what to do with: %c", line[i])
				}
			case 'n':
				pos.Y++
				i++
				switch line[i] {
				case 'e':
					pos.X++
				case 'w':
					pos.X--
				default:
					return nil, tools.Pos2D{}, tools.Pos2D{}, fmt.Errorf("no clue what to do with: %c", line[i])
				}
			default:
				return nil, tools.Pos2D{}, tools.Pos2D{}, fmt.Errorf("no clue what to do with: %c", line[i])
			}
		}
		newVal := !isBlack[pos]
		isBlack[pos] = newVal
		if newVal {
			if pos.X > max.X {
				max.X = pos.X
			}
			if pos.Y > max.Y {
				max.Y = pos.Y
			}
			if pos.X < min.X {
				min.X = pos.X
			}
			if pos.Y < min.Y {
				min.Y = pos.Y
			}
		}
	}
	return isBlack, min, max, nil
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	isBlack, _, _, err := load(lines)
	if err != nil {
		return "", err
	}

	out := 0
	for _, val := range isBlack {
		if val {
			out++
		}
	}

	return strconv.Itoa(out), nil // Return with the answer
}
