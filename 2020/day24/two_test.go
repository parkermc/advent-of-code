package day24

import "testing"

func TestTwo(t *testing.T) {
	answer, err := Two(testFilename)
	if err != nil {
		t.Fatal(err)
	}
	if answer != testAnswerTwo {
		t.Fatalf("Answer should be \"%s\" but was \"%s\"", testAnswerTwo, answer)
	}
}
