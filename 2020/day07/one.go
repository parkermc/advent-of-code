package day07

import (
	"errors"
	"regexp"
	"strconv"

	"github.com/golang-collections/go-datastructures/set"
	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

var bagReg = regexp.MustCompile(`^(.+) (?:bags|bag) contain`)
var containsReg = regexp.MustCompile(`(?:(\d+) ([^,.]+) (?:bags|bag)[,.]|no other)`)

func getParents(bagMap map[string][]string, parentsOf string) *set.Set {
	s := set.New()
	getParentsRe(bagMap, parentsOf, s)
	return s
}

func getParentsRe(bagMap map[string][]string, parentsOf string, s *set.Set) {
	for _, bag := range bagMap[parentsOf] {
		s.Add(bag)
		getParentsRe(bagMap, bag, s)
	}
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	bagMap := make(map[string][]string, len(lines))
	for _, line := range lines {
		bagMatch := bagReg.FindStringSubmatch(line)
		if len(bagMatch) == 0 {
			return "", errors.New("data error when using regex")
		}

		containsMatch := containsReg.FindAllStringSubmatch(line, -1)
		if len(containsMatch) == 0 {
			return "", errors.New("data error when using regex")
		}
		for _, subMatch := range containsMatch {
			if subMatch[2] != "" {
				if bagMap[subMatch[2]] == nil {
					bagMap[subMatch[2]] = make([]string, 0)
				}
				bagMap[subMatch[2]] = append(bagMap[subMatch[2]], bagMatch[1])
			}
		}
	}

	parents := getParents(bagMap, "shiny gold")

	return strconv.FormatInt(parents.Len(), 10), nil // Return with the answer
}
