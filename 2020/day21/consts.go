// Package day21 2020
package day21

import "gitlab.com/parkermc/advent-of-code/2020/tools"

const testAnswerOne = "2098"
const testAnswerTwo = "ppdplc,gkcplx,ktlh,msfmt,dqsbql,mvqkdj,ggsz,hbhsx"

var testFilename = "../test/private/" + tools.GetPackageName()
