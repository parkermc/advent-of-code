package day06

import (
	"strconv"

	"github.com/golang-collections/go-datastructures/bitarray"
	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRequireLast(filename)
	if err != nil {
		return "", err
	}

	count := 0
	var group bitarray.BitArray
	for _, line := range lines {
		if line == "" {
			for i := uint64(0); i < 27; i++ {
				bit, err := group.GetBit(i)
				if err != nil {
					return "", err
				}
				if bit {
					count++
				}
			}
			group = nil
			continue
		}
		person := bitarray.NewBitArray(26)
		for _, c := range line {
			err := person.SetBit(uint64(c) - 97)
			if err != nil {
				return "", err
			}
		}
		if group == nil {
			group = person
		} else {
			group = group.And(person)
		}
	}

	return strconv.Itoa(count), nil // Return with the answer
}
