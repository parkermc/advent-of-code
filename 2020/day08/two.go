package day08

import (
	"errors"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	program, err := convertToProgram(lines)
	if err != nil {
		return "", nil
	}

	n := 0
	var accumulator int
	good := false
	for !good {
		for program[n].OP == OPTypeACC {
			n++
		}
		switch program[n].OP {
		case OPTypeJMP:
			program[n].OP = OPTypeNOP
		case OPTypeNOP:
			program[n].OP = OPTypeJMP
		default:
			return "", errors.New("invalid OP code to swap")
		}
		accumulator, _, good = runUntilRepeat(program)
		switch program[n].OP {
		case OPTypeJMP:
			program[n].OP = OPTypeNOP
		case OPTypeNOP:
			program[n].OP = OPTypeJMP
		default:
			return "", errors.New("invalid OP code to swap")
		}
		n++
	}

	return strconv.Itoa(accumulator), nil // Return with the answer
}
