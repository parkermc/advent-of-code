// Package day08 2020
package day08

import "gitlab.com/parkermc/advent-of-code/2020/tools"

const testAnswerOne = "2003"
const testAnswerTwo = "1984"

var testFilename = "../test/private/" + tools.GetPackageName()
