package day01

import (
	"errors"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	nums := make([]int, len(lines))
	for i, line := range lines {
		nums[i], err = strconv.Atoi(line)
		if err != nil {
			return "", err
		}
	}

	for i, num1 := range nums {
		for j, num2 := range nums[i+1:] {
			for _, num3 := range nums[j+1:] {
				if num1+num2+num3 == 2020 {
					return strconv.Itoa(num1 * num2 * num3), nil
				}
			}
		}
	}

	return "", errors.New("invalid input") // Return with the answer
}
