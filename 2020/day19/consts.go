// Package day19 2020
package day19

import "gitlab.com/parkermc/advent-of-code/2020/tools"

const testAnswerOne = "269"
const testAnswerTwo = "403"

var testFilename = "../test/private/" + tools.GetPackageName()
