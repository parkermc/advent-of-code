// Package day10 2020
package day10

import "gitlab.com/parkermc/advent-of-code/2020/tools"

const testAnswerOne = "2263"
const testAnswerTwo = "396857386627072"

var testFilename = "../test/private/" + tools.GetPackageName()
