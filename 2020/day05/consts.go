// Package day05 2020
package day05

import "gitlab.com/parkermc/advent-of-code/2020/tools"

const testAnswerOne = "959"
const testAnswerTwo = "527"

var testFilename = "../test/private/" + tools.GetPackageName()
