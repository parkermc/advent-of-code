package day03

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

func countTrees(lines []string, colSkip, rowSkip int) int {
	row, col := 0, 0
	trees := 0
	for {
		if lines[row][col] == '#' {
			trees++
		}
		row += rowSkip
		col += colSkip
		if row >= len(lines) {
			return trees
		}
		if col >= len(lines[row]) {
			col -= len(lines[row])
		}
	}
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	return strconv.Itoa(countTrees(lines, 3, 1)), nil // Return with the answer
}
