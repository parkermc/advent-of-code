// Package day15 2020
package day15

import "gitlab.com/parkermc/advent-of-code/2020/tools"

const testAnswerOne = "403"
const testAnswerTwo = "6823"

var testFilename = "../test/private/" + tools.GetPackageName()
