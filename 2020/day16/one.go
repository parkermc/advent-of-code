package day16

import (
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

var firstReg = regexp.MustCompile(`^([^:]+): (\d+)-(\d+) or (\d+)-(\d+)$`)

type fieldT struct {
	name string
	min1 int
	max1 int
	min2 int
	max2 int
}

func loadData(lines []string) ([]fieldT, []int, [][]int, error) {
	var err error
	fields := make([]fieldT, 0)
	var myTicket []int
	var tickets [][]int
	ticketNum := 0
	loadingMode := 0
	for i, line := range lines {
		switch loadingMode {
		case 0:
			if line == "" {
				break
			}
			if line == "your ticket:" {
				loadingMode++
				break
			}
			match := firstReg.FindStringSubmatch(line)
			min1, err := strconv.Atoi(match[2])
			if err != nil {
				return nil, nil, nil, err
			}
			max1, err := strconv.Atoi(match[3])
			if err != nil {
				return nil, nil, nil, err
			}
			min2, err := strconv.Atoi(match[4])
			if err != nil {
				return nil, nil, nil, err
			}
			max2, err := strconv.Atoi(match[5])
			if err != nil {
				return nil, nil, nil, err
			}
			fields = append(fields, fieldT{name: match[1], min1: min1, max1: max1, min2: min2, max2: max2})
		case 1:
			if line == "" {
				break
			}
			if line == "nearby tickets:" {
				loadingMode++
				tickets = make([][]int, len(lines)-i-1)
				break
			}
			split := strings.Split(line, ",")
			myTicket = make([]int, len(split))
			for j, strVal := range split {
				myTicket[j], err = strconv.Atoi(strVal)
				if err != nil {
					return nil, nil, nil, err
				}
			}
		case 2:
			split := strings.Split(line, ",")
			ticket := make([]int, len(split))
			for j, strVal := range split {
				val, err := strconv.Atoi(strVal)
				if err != nil {
					return nil, nil, nil, err
				}
				ticket[j] = val

			}
			tickets[ticketNum] = ticket
			ticketNum++
		}
	}
	return fields, myTicket, tickets, nil
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	out := 0
	validNumsLargest := 0
	fields, _, tickets, err := loadData(lines)
	if err != nil {
		return "", err
	}

	for _, field := range fields {
		if field.max1 > validNumsLargest {
			validNumsLargest = field.max1
		}
		if field.max2 > validNumsLargest {
			validNumsLargest = field.max2
		}
	}
	validNums := make([]bool, validNumsLargest+1)
	for _, field := range fields {
		for _, bounds := range [][]int{{field.min1, field.max1}, {field.min2, field.max2}} {
			for i := bounds[0]; i <= bounds[1]; i++ {
				validNums[i] = true
			}
		}
	}

	for _, ticket := range tickets {
		for _, val := range ticket {
			if val > validNumsLargest || !validNums[val] {
				out += val
			}
		}
	}

	return strconv.Itoa(out), nil // Return with the answer
}
