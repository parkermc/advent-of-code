// Package day25 2020
package day25

import "gitlab.com/parkermc/advent-of-code/2020/tools"

const testAnswerOne = "448851"

var testFilename = "../test/private/" + tools.GetPackageName()
