package day14

import (
	"fmt"
	"regexp"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

var reg = regexp.MustCompile(`(mask|mem)(?:\[(\d+)\])? = ([X\d]*)`)

func setBit(n int64, pos int) int64 {
	n |= (1 << pos)
	return n
}

func clearBit(n int64, pos int) int64 {
	mask := int64(^(1 << pos))
	n &= mask
	return n
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	bitmask := ""
	nums := make(map[int]int64)
	for _, line := range lines {
		match := reg.FindStringSubmatch(line)
		switch match[1] {
		case "mask":
			bitmask = match[3]
		case "mem":
			num, err := strconv.ParseInt(match[3], 10, 0)
			if err != nil {
				return "", err
			}
			for i, c := range bitmask {
				switch c {
				case 'X':
				case '0':
					num = clearBit(num, 35-i)
				case '1':
					num = setBit(num, 35-i)
				default:
					return "", fmt.Errorf("invalid bitmask char: %c", c)
				}
			}
			i, err := strconv.Atoi(match[2])
			if err != nil {
				return "", err
			}
			nums[i] = num
		default:
			return "", fmt.Errorf("unknown thing to set: %s", match[1])
		}
	}

	sum := int64(0)
	for _, val := range nums {
		sum += val
	}

	return strconv.FormatInt(sum, 10), nil // Return with the answer
}
