// Package day14 2020
package day14

import "gitlab.com/parkermc/advent-of-code/2020/tools"

const testAnswerOne = "4886706177792"
const testAnswerTwo = "3348493585827"

var testFilename = "../test/private/" + tools.GetPackageName()
