package day12

import (
	"fmt"
	"math"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

func toRad(angle int) float64 {
	return float64(angle) * math.Pi / 180
}

func rotatePoint(pos *tools.Pos2D, angle int) {
	rAngle := toRad(angle)
	s := math.Sin(rAngle)
	c := math.Cos(rAngle)

	// Rotate point
	newX := float64(pos.X)*c - float64(pos.Y)*s
	newY := float64(pos.X)*s + float64(pos.Y)*c

	pos.X = int(math.Round(newX))
	pos.Y = int(math.Round(newY))

}

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	waypoint := tools.Pos2D{X: 10, Y: 1}
	pos := tools.Pos2D{X: 0, Y: 0}
	for _, line := range lines {
		val, err := strconv.Atoi(line[1:])
		if err != nil {
			return "", err
		}
		switch line[0] {
		case 'N':
			waypoint.Y += val
		case 'S':
			waypoint.Y -= val
		case 'E':
			waypoint.X += val
		case 'W':
			waypoint.X -= val
		case 'L':
			rotatePoint(&waypoint, val)
		case 'R':
			rotatePoint(&waypoint, -val)
		case 'F':
			pos.X += waypoint.X * val
			pos.Y += waypoint.Y * val
		default:
			return "", fmt.Errorf("unknown action: %c", line[0])
		}
	}

	return strconv.Itoa(tools.Abs(pos.X) + tools.Abs(pos.Y)), nil // Return with the answer
}
