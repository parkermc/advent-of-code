// Package day20 2020
package day20

import "gitlab.com/parkermc/advent-of-code/2020/tools"

const testAnswerOne = "17032646100079"
const testAnswerTwo = "2006"

var testFilename = "../test/private/" + tools.GetPackageName()
